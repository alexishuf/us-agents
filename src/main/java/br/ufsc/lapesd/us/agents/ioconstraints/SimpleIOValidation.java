package br.ufsc.lapesd.us.agents.ioconstraints;

import br.ufsc.lapesd.us.agents.RDFData;

/**
 * Result of validating inputs.
 */
public class SimpleIOValidation implements IOValidation {
    private final boolean valid;
    private final RDFData explanation;

    public SimpleIOValidation(boolean valid, RDFData explanation) {
        this.valid = valid;
        this.explanation = explanation;
    }

    @Override
    public boolean isValid() {
        return valid;
    }

    @Override
    public RDFData getExplanation() {
        return explanation;
    }
}
