package br.ufsc.lapesd.us.agents.ioconstraints;

import br.ufsc.lapesd.us.agents.RDFData;
import br.ufsc.lapesd.us.agents.desires.Desire;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

/**
 * Allows validation of input data and verification of chaining
 */
public interface IOConstraints {
    /**
     * Validates particular RDF data against this constraint object.
     *
     * @param  desire Desire that is to be fulfilled using the given inputData
     * @param inputData RDF data that is to be tested.
     * @return a report of the results
     */
    @Nonnull IOValidation validateInput(@Nonnull Desire desire, @Nullable RDFData inputData);

    /**
     * Computes an example output data RDF model. This data is bogus and should not
     * be treated as valid data. It can however be used in as the input argument of future
     * validateInput calls.
     *
     * @param myDesire Consider the output that would be produced by fulfilling this Desire in
     *                 this constraints object.
     * @return Example triples
     */
    RDFData exampleOutput(@Nonnull Desire myDesire);
}
