package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.*;
import br.ufsc.lapesd.us.agents.exception.FulfillmentException;
import br.ufsc.lapesd.us.agents.exception.NoProviderException;
import br.ufsc.lapesd.us.agents.ioconstraints.IOValidation;
import br.ufsc.lapesd.us.agents.ioconstraints.WorkflowPlaner;
import br.ufsc.lapesd.us.agents.ioconstraints.WorkflowValidation;
import br.ufsc.lapesd.us.agents.priv.RelationHelper;
import br.ufsc.lapesd.us.agents.exception.MissingInputsException;
import org.apache.jena.ext.com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.HashMap;
import java.util.List;

/**
 * A composing agent fulfills a desire using not
 * {@link DesireExecutor} instances, but other agents
 */
public class ComposingAgentImpl implements DesireProvider {
    private final Desire provided;
    private final ParticipationRules participationRules;
    private final List<Desire> relatedDesires;
    private final ProviderSelector selector;
    private final RelationHelper relationHelper;
    private long load = 0;
    private WorkflowPlaner planner = null;

    private class Step {
        public final Desire desire;
        public final DesireProvider provider;

        public Step(Desire desire, DesireProvider provider) {
            this.desire = desire;
            this.provider = provider;
        }
    }

    public ComposingAgentImpl(Desire provided, String consolidatedOntology,
                              List<Desire> relatedDesires, ProviderSelector selector) throws NoProviderException {
        this.provided = provided;
        this.relatedDesires = relatedDesires;
        this.selector = selector;
        this.relationHelper = new RelationHelper(this);
        participationRules = ParticipationRulesFactory.getInstance()
                .create(provided, consolidatedOntology);
        planner = cretePlaner();
    }

    @Override
    public String getName() {
        return String.format("ComposingAgent(%1$s)", provided);
    }

    @Override
    public RDFData fulfill(@Nonnull Desire desire, @Nonnull RDFData inputData,
                        @Nullable IOValidation validation) throws FulfillmentException {
        Preconditions.checkArgument(!queryParticipation(desire).isUnrelated());

        if (validation == null || !(validation instanceof WorkflowValidation))
            validation = validateInput(desire, inputData);
        if (!validation.isValid())
            throw new MissingInputsException(validation.getExplanation());

        List<WorkflowValidation.Step> workflow = ((WorkflowValidation) validation).getWorkflow();
        Model rolling = inputData.createModel();
        Model output = ModelFactory.createDefaultModel();
        for (WorkflowValidation.Step step : workflow)
            step.fulfill(inputData).with(rolling::add, output::add);

        //TODO handle failures
        return RDFData.toTurtle(output);
    }

    private WorkflowPlaner cretePlaner() throws NoProviderException {
        HashMap<Desire, List<DesireProvider>> map = new HashMap<>();
        for (Desire related : relatedDesires) {
            List<DesireProvider> selected = selector.select(related);
            if (selected.isEmpty())
                throw new NoProviderException(related);
            map.put(related, selected);
        }
        return new WorkflowPlaner(map);
    }

    @Override
    public Participation queryParticipation(@Nonnull Desire desire) {
        return participationRules.queryParticipation(desire, load);
    }

    @Override
    public ParticipationContainment relationWith(@Nonnull DesireProvider other, @Nonnull Desire desire) {
        Participation participation = queryParticipation(desire);
        return relationHelper.relationWith(provided, participation, other, desire);
    }

    @Override
    public @Nonnull IOValidation validateInput(@Nonnull Desire desire, RDFData inputData) {
        Preconditions.checkArgument(!queryParticipation(desire).isUnrelated());
        return planner.validateInput(desire, inputData);
    }

    @Override
    public RDFData exampleOutput(@Nonnull Desire myDesire) {
        return planner.exampleOutput(myDesire);
    }

    private synchronized long incLoad() {
        return load++;
    }

    private synchronized long decLoad() {
        return load--;
    }
}
