package br.ufsc.lapesd.us.agents.desires;

/**
 * Represents a desire.
 *
 * A desire is always applied to an {@link InformationElement}, and it can be compared with
 * other desires, either for traditional equality (equivalentTo), or for testing if the
 * desires are the same ignoring the informationElement()s (sameDesire()).
 */
public interface Desire {
    /**
     * Tests if, ignoring the information elements, this desire is equal to other.
     * @param other Other desire to test agains
     * @return true iff other is the same desire, ignring informationElement()
     */
    boolean sameDesire(Desire other);

    /**
     * Equivalent to
     * <code>sameDesire(other) && informationElement() == other.informationElement()</code>.
     */
    boolean equivalentTo(Desire other);

    /**
     * The desire information element.
     * @return
     */
    InformationElement informationElement();

    boolean equals(Object other);

    int hashCode();
}
