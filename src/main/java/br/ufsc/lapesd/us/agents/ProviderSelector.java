package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.Desire;

import java.util.List;

/**
 * Created by alexis on 7/15/16.
 */
public interface ProviderSelector {
    void addProvider(DesireProvider provider);

    void removeProvider(DesireProvider provider);

    List<DesireProvider> select(Desire desire);
}
