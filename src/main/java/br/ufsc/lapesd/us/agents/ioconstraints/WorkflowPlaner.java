package br.ufsc.lapesd.us.agents.ioconstraints;

import br.ufsc.lapesd.us.agents.DesireProvider;
import br.ufsc.lapesd.us.agents.RDFData;
import br.ufsc.lapesd.us.agents.desires.Desire;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * {@link IOConstraints} implementation that considers the need to perform several sub-tasks.
 * During the input validation process, a workflow will be built and returned in the form of a
 * {@link WorkflowValidation}. The resulting validation object may be used as a workflow later.
 */
public class WorkflowPlaner implements IOConstraints {
    private final HashMap<Desire, List<DesireProvider>> tasks;

    public WorkflowPlaner(HashMap<Desire, List<DesireProvider>> tasks) {
        this.tasks = tasks;
    }

    /**
     * Validates if inputData is sufficient to perform all sub-tasks.
     *
     * @param  desire Desire that is to be fulfilled using the given inputData
     * @param inputData RDF data that is to be tested.
     * @return a {@link WorkflowValidation} that will contain a workflow, if inputData is valid.
     */
    @Override
    public @Nonnull WorkflowValidation validateInput(@Nonnull Desire desire,  @Nullable RDFData inputData) {
        HashSet<ProtoStep> unvisited = createProtoSteps();
        List<List<WorkflowValidation.Step>> layers = new ArrayList<>();
        Model rolling = inputData != null ? inputData.createModel()
                : ModelFactory.createDefaultModel();

        while (!unvisited.isEmpty()) {
            List<WorkflowValidation.Step> layer = new ArrayList<>();
            List<ProtoStep> visited = new ArrayList<>();
            for (ProtoStep protoStep : unvisited) {
                WorkflowValidation.Step step = protoStep.validateInput(rolling);
                if (step != null) {
                    layer.add(step);
                    visited.add(protoStep);
                }
            }
            unvisited.removeAll(visited);
            if (layer.isEmpty()) {
                RDFData explanation = RDFData.toTurtle(ModelFactory.createDefaultModel());
                return new WorkflowValidation(false, explanation, Collections.emptyList());
            }
            for (WorkflowValidation.Step step : layer) {
                RDFData rdf = step.getProvider().exampleOutput(step.getDesire());
                rdf.with(rolling::add);
            }
            layers.add(layer);
        }
        rolling.close();

        List<WorkflowValidation.Step> workflow = new ArrayList<>();
        layers.forEach(l -> l.forEach(workflow::add));
        return new WorkflowValidation(true, null, workflow);
    }

    @Override
    public RDFData exampleOutput(@Nonnull Desire myDesire) {
        Model combined = ModelFactory.createDefaultModel();
        for (Map.Entry<Desire, List<DesireProvider>> entry : tasks.entrySet()) {
            for (DesireProvider provider : entry.getValue()) {
                RDFData rdf = provider.exampleOutput(entry.getKey());
                rdf.with(combined::add);
            }
        }
        return RDFData.toTurtle(combined);
    }

    private HashSet<ProtoStep> createProtoSteps() {
        HashSet<ProtoStep> set = new HashSet<>();
        for (Map.Entry<Desire, List<DesireProvider>> entry : tasks.entrySet()) {
            set.addAll(entry.getValue().stream()
                    .map(provider -> new ProtoStep(entry.getKey(), provider))
                    .collect(Collectors.toList()));
        }
        return set;
    }

    private static class ProtoStep {
        public final Desire desire;
        public final DesireProvider provider;

        public ProtoStep(Desire desire, DesireProvider provider) {
            this.desire = desire;
            this.provider = provider;
        }

        WorkflowValidation.Step validateInput(Model inputModel) {
            RDFData data = RDFData.fromModel(inputModel, Lang.TURTLE);
            IOValidation validation = provider.validateInput(desire, data);
            if (!validation.isValid()) return null;
            return new WorkflowValidation.Step(desire, provider, validation);
        }
    }
}
