package br.ufsc.lapesd.us.agents.ioconstraints;

import br.ufsc.lapesd.us.agents.RDFData;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.DesireExecutor;
import br.ufsc.lapesd.us.agents.vocab.SD;
import com.google.common.base.Preconditions;
import org.apache.jena.graph.Node;
import org.apache.jena.graph.Triple;
import org.apache.jena.rdf.model.*;
import org.apache.jena.sparql.core.TriplePath;
import org.apache.jena.sparql.syntax.*;
import org.apache.jena.vocabulary.OWL;
import org.apache.jena.vocabulary.RDF;
import org.topbraid.spin.arq.ARQFactory;
import org.topbraid.spin.arq.AbstractElementVisitor;
import org.topbraid.spin.constraints.ConstraintViolation;
import org.topbraid.spin.constraints.SPINConstraints;
import org.topbraid.spin.model.Ask;
import org.topbraid.spin.model.Query;
import org.topbraid.spin.model.QueryOrTemplateCall;
import org.topbraid.spin.model.SPINFactory;
import org.topbraid.spin.system.SPINModuleRegistry;

import javax.annotation.Nonnull;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Interprets SPIN documentation of
 * {@link DesireExecutor} instances.
 */
public class DesireExecutorIOConstraints implements IOConstraints {
    private static boolean spinInitialized = false;
    private final Model descriptionModel;
    private final Resource description;
    private final DesireExecutor executor;
    private Query input = null;
    private Ask output = null;

    public DesireExecutorIOConstraints(DesireExecutor executor) {
        this.executor = executor;

        initSpin(); //TODO move to a better place

        description = this.executor.getServiceDescription();
        descriptionModel = description == null ? null : description.getModel();
        input = getConstraint(description, SD.inputConstraints);
        try {
            output = (Ask) getConstraint(description, SD.outputConstraints);
        } catch (ClassCastException ignored) { /* pass */ }
    }

    private static synchronized void initSpin() {
        if (spinInitialized) return;
        spinInitialized = true;
        SPINModuleRegistry.get().init();
    }

    private Query getConstraint(Resource description, Property property) {
        if (description == null) return null;
        Query query = null;
        StmtIterator it = description.listProperties(property);
        while (it.hasNext()) {
            RDFNode node = it.next().getObject();
            if (!node.isResource()) continue;
            try {
                query = SPINFactory.asQuery(node.asResource());
                break; //get first
            } catch (ClassCastException ignored) { /* pass */ }
        }
        return query;
    }

    @Override
    public @Nonnull IOValidation validateInput(Desire desire, RDFData inputData) {
        if (input == null) return new SimpleIOValidation(true, null);

        Model union = inputData.createModel();
        union.add(descriptionModel);
        List<ConstraintViolation> violations = new ArrayList<>();
        Resource unionDescription = description.isAnon()
                ? union.createResource(description.getId())
                : union.createResource(description.toString());
        QueryOrTemplateCall qot = new QueryOrTemplateCall(unionDescription, input);
        SPINConstraints.addQueryResults(violations, qot, unionDescription, true, null, null);
        union.close();

        Model explanation = ModelFactory.createDefaultModel();
        SPINConstraints.addConstraintViolationsRDF(violations, explanation, true);

        return new SimpleIOValidation(violations.isEmpty(), RDFData.toTurtle(explanation));
    }

    @Override
    public RDFData exampleOutput(@Nonnull Desire myDesire) {
        Model outputModel = ModelFactory.createDefaultModel();
        if (this.output != null)
            fillDummyOutput(outputModel);
        return RDFData.toTurtle(outputModel);
    }

    private class NodeManager {
        private HashMap<Node, RDFNode> map = new HashMap<>();
        private Model output;

        public NodeManager(Model output) {
            this.output = output;
        }

        public Resource getResource(Node node) {
            Preconditions.checkArgument(!node.isLiteral());
            if (node.isVariable()) {
                RDFNode rdfNode = map.getOrDefault(node, null);
                if (rdfNode == null) {
                    rdfNode = output.createResource();
                    map.put(node, rdfNode);
                }
                return rdfNode.asResource();
            }
            return output.createResource(node.getURI());
        }

        public RDFNode getNode(Node node) {
            return !node.isLiteral() ? getResource(node)
                    : output.createLiteral(node.getLiteralLexicalForm(), false);
        }

        public Property getProperty(Node node, boolean dataProperty) {
            RDFNode rdfNode = map.getOrDefault(node, null);
            if (rdfNode == null || !rdfNode.asResource().canAs(Property.class)) {
                rdfNode = getResource(node);
                output.add((Resource)rdfNode, RDF.type, RDF.Property);
                output.add((Resource)rdfNode, RDF.type,
                        dataProperty ? OWL.DatatypeProperty : OWL.ObjectProperty);
            }
            return rdfNode.as(Property.class);
        }
    }

    private void fillDummyOutput(Model model) {
        org.apache.jena.query.Query query = ARQFactory.get().createQuery(output);
        Element queryPattern = query.getQueryPattern();
        if (queryPattern == null) return;
        NodeManager nodeMgr = new NodeManager(model);
        queryPattern.visit(new AbstractElementVisitor() {
            @Override
            public void visit(ElementPathBlock pathBlock) {
                for (TriplePath t : pathBlock.getPattern()) {
                    processBGP(t.getSubject(), t.getPredicate(), t.getObject());
                }
            }

            @Override
            public void visit(ElementTriplesBlock el) {
                for (Triple t : el.getPattern()) {
                    processBGP(t.getSubject(), t.getPredicate(), t.getObject());
                }
            }

            private void processBGP(Node tSubj, Node tProperty, Node tObject) {
                Resource subj = nodeMgr.getResource(tSubj);
                RDFNode object = nodeMgr.getNode(tObject);
                Property property = nodeMgr.getProperty(tProperty,
                        object.isLiteral());
                model.add(subj, property, object);
            }
        });
    }
}
