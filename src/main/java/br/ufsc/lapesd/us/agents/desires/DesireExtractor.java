package br.ufsc.lapesd.us.agents.desires;

import java.util.HashMap;
import java.util.List;
import java.util.function.Supplier;

public class DesireExtractor {
    private static final DesireExtractor instance = new DesireExtractor();
    private HashMap<String, Supplier<List<DesireExecutor>>> forced = new HashMap<>();

    private DesireExtractor() { }

    public static DesireExtractor getInstance() {
        return instance;
    }

    public void forceExtractor(String entryPoint, Supplier<List<DesireExecutor>> supplier) {
        forced.put(entryPoint, supplier);
    }

    /**
     * Fetches a service description for the given service and returns a list with executors
     * that fulfill desires using the service.
     * @param serviceEntryPoint entrypoint of the service, that can be used to find it's
     *                          description
     * @return List of executors that fulfill desires using the service.
     */
    public List<DesireExecutor> createExecutors(String serviceEntryPoint) {
        Supplier<List<DesireExecutor>> supplier = forced.getOrDefault(serviceEntryPoint, null);
        if (supplier != null)
            return supplier.get();
        throw new UnsupportedOperationException("Not implemented");
    }

    /**
     * Undoes the effect of any previous forceExtractor() call.
     */
    public void clearForced() {
        forced.clear();
    }
}
