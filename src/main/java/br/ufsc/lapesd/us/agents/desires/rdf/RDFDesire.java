package br.ufsc.lapesd.us.agents.desires.rdf;

import br.ufsc.lapesd.us.agents.desires.InformationElement;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.vocab.USA;
import com.google.common.base.Preconditions;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.NodeIterator;
import org.apache.jena.rdf.model.RDFNode;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.vocabulary.RDF;
import org.apache.jena.vocabulary.RDFS;

import java.util.Stack;

/**
 * Implementation of an Desire backed by a RDF class.
 */
public class RDFDesire implements Desire {
    private String iri;
    private RDFInformationElement informationElement;

    public RDFDesire(String classIri, RDFInformationElement informationElement) {
        this.iri = classIri;
        this.informationElement = informationElement;
    }

    public static RDFDesire fromResource(Resource resource) {
        Preconditions.checkNotNull(resource);
        Model model = resource.getModel();
        Preconditions.checkArgument(model != null);

        String classIri = null;
        NodeIterator it = model.listObjectsOfProperty(resource, RDF.type);
        while (it.hasNext()) {
            RDFNode node = it.next();
            if (!node.isResource()) continue;
            if (!isDesireSubClass(node.asResource())) continue;
            classIri = node.asResource().toString();
            break; //stop at first
        }
        if (classIri == null) return null;

        String infElement = null;
        it = model.listObjectsOfProperty(resource, USA.informationElement);
        while (it.hasNext()) {
            RDFNode node = it.next();
            if (!node.isResource()) continue;
            infElement = node.asResource().toString();
            break; //stop at first
        }
        if (infElement == null) return null;

        return new RDFDesire(classIri, new RDFInformationElement(infElement));
    }

    private static boolean isDesireSubClass(Resource resource) {
        Preconditions.checkNotNull(resource);
        Model model = resource.getModel();
        Preconditions.checkArgument(model != null);

        Stack<Resource> stack = new Stack<>();
        stack.push(resource);
        while (!stack.isEmpty()) {
            Resource popped = stack.pop();
            if (popped.equals(USA.Desire) || popped.equals(USA.CompositeDesire))
                return true;
            NodeIterator it = model.listObjectsOfProperty(popped, RDFS.subClassOf);
            while (it.hasNext()) {
                RDFNode node = it.next();
                if (!node.isResource()) continue;
                stack.push(node.asResource());
            }
        }

        return false;
    }

    @Override
    public boolean sameDesire(Desire other) {
        try {
            RDFDesire otherRDF = (RDFDesire) other;
            return iri.equals(otherRDF.iri);
        } catch (ClassCastException e) {
            return false;
        }
    }

    @Override
    public boolean equivalentTo(Desire other) {
        return sameDesire(other) && informationElement().equals(other.informationElement());
    }

    public InformationElement informationElement() {
        return informationElement;
    }

    @Override
    public boolean equals(Object o) {
        try {
            return equivalentTo((Desire)o);
        } catch (ClassCastException ignored) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(iri).append(informationElement).hashCode();
    }

    @Override
    public String toString() {
        return String.format("RDFDesire(%1$s, %2$s)", iri, informationElement.toString());
    }
}
