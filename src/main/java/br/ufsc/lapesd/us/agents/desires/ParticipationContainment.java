package br.ufsc.lapesd.us.agents.desires;

/**
 * Relation between two DesireProviders with respect to providing a particular desire.
 */
public enum ParticipationContainment {
    /**
     * Both providers provide equivalent functionality
     */
    EQUIVALENT,
    /**
     * The left-hand sided provider functionality is contained in the functionality of the
     * right-handed provider.
     */
    CONTAINED,
    /**
     * The left-hand provider functionality includes that of the left-hand one.
     */
    CONTAINS,
    /**
     * Nothing can be affirmed about containment relation.
     */
    IRRELEVANT,
}
