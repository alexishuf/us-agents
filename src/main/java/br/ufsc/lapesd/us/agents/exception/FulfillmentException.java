package br.ufsc.lapesd.us.agents.exception;

/**
 * An error that avoided the fulfillment of a desire.
 */
public abstract class FulfillmentException extends Exception {
    public FulfillmentException() {
    }

    public FulfillmentException(String s) {
        super(s);
    }

    public FulfillmentException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public FulfillmentException(Throwable throwable) {
        super(throwable);
    }
}
