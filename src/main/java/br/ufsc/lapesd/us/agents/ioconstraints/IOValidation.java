package br.ufsc.lapesd.us.agents.ioconstraints;

import br.ufsc.lapesd.us.agents.RDFData;

import java.io.Serializable;

/**
 * Result from validating input data against IO constraints.
 */
public interface IOValidation extends Serializable {
    boolean isValid();

    RDFData getExplanation();
}
