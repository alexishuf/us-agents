package br.ufsc.lapesd.us.agents.desires.rdf;

import br.ufsc.lapesd.us.agents.exception.NoProviderException;
import br.ufsc.lapesd.us.agents.ComposingAgentImpl;
import br.ufsc.lapesd.us.agents.ProviderSelector;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.vocab.USA;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;

import java.util.ArrayList;
import java.util.List;

/**
 * Creates {@link ComposingAgentImpl} instances from usa:CompositeDesire
 * RDF instances.
 */
public class RDFComposingAgentImplFactory {
    private static RDFComposingAgentImplFactory instance = new RDFComposingAgentImplFactory();

    public static RDFComposingAgentImplFactory getInstance() {
        return instance;
    }

    public List<Desire> getRelatedDesires(Resource compositeDesire) {
        Preconditions.checkNotNull(compositeDesire);
        Model model = compositeDesire.getModel();
        Preconditions.checkArgument(model != null);

        List<Desire> related = new ArrayList<>();
        NodeIterator it = model.listObjectsOfProperty(compositeDesire, USA.relatedDesire);
        while (it.hasNext()) {
            RDFNode node = it.next();
            if (!node.isResource()) continue;
            RDFDesire desire = RDFDesire.fromResource(node.asResource());
            related.add(desire);
        }
        return related;
    }

    public ComposingAgentImpl createAgentImpl(Resource compositeDesire,
                                              String consolidatedOntology,
                                              ProviderSelector selector)
            throws NoProviderException {
        Desire provided = RDFDesire.fromResource(compositeDesire);
        List<Desire> related = getRelatedDesires(compositeDesire);
        return new ComposingAgentImpl(provided, consolidatedOntology, related, selector);
    }

    public List<Resource> getCompositeDesires(Model model) {
        List<Resource> list = new ArrayList<>();
        ResIterator it = model.listSubjectsWithProperty(USA.relatedDesire);
        while (it.hasNext()) list.add(it.next());
        return list;
    }
}
