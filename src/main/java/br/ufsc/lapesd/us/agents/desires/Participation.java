package br.ufsc.lapesd.us.agents.desires;

import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.io.Serializable;

public class Participation implements Serializable {
    private final ParticipationType participationType;
    private final long providerLoad;

    public Participation(ParticipationType participationType, long providerLoad) {
        this.participationType = participationType;
        this.providerLoad = providerLoad;
    }

    public ParticipationType getParticipationType() {
        return participationType;
    }

    public boolean isComplete() {
        return participationType.equals(ParticipationType.COMPLETE);
    }

    public boolean isPartial() {
        return participationType.equals(ParticipationType.PARTIAL);
    }

    public boolean isUnrelated() {
        return participationType.equals(ParticipationType.UNRELATED);
    }

    public long getProviderLoad() {
        return providerLoad;
    }

    public boolean betterThan(Participation other) {
        int cmp = getParticipationType().compareTo(other.getParticipationType());
        if (cmp != 0) return cmp > 0;
        else return getProviderLoad() < other.getProviderLoad();
    }

    @Override
    public boolean equals(Object o) {
        if (!(o instanceof Participation)) return false;
        Participation rhs = (Participation) o;
        return getParticipationType().equals(rhs.getParticipationType()) &&
                providerLoad == rhs.getProviderLoad();
    }

    @Override
    public int hashCode() {
        return new HashCodeBuilder().append(participationType).append(providerLoad).hashCode();
    }
}
