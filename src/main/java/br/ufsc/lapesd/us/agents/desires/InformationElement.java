package br.ufsc.lapesd.us.agents.desires;

import java.util.Objects;

/**
 * Represents a information element. Must implement equals and hashcode
 */
public interface InformationElement {
    boolean equals(Object other);
    int hashCode();
}
