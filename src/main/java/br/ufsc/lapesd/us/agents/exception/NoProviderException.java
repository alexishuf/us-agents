package br.ufsc.lapesd.us.agents.exception;

import br.ufsc.lapesd.us.agents.desires.Desire;

/**
 * No provider could be found for a Desire.
 */
public class NoProviderException extends FulfillmentException {
    public NoProviderException(Desire desire) {
        super(String.format("No provider for desire %1$s could be found.", desire));
    }
}
