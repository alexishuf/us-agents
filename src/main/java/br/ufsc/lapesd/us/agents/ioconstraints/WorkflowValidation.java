package br.ufsc.lapesd.us.agents.ioconstraints;

import br.ufsc.lapesd.us.agents.DesireProvider;
import br.ufsc.lapesd.us.agents.RDFData;
import br.ufsc.lapesd.us.agents.exception.FulfillmentException;
import br.ufsc.lapesd.us.agents.desires.Desire;

import java.util.List;

/**
 * A {@link IOValidation} produced by {@link WorkflowPlaner}. When the validation was
 * successful, also contains a linear workflow plan.
 */
public class WorkflowValidation extends SimpleIOValidation {
    private final List<Step> workflow;

    public WorkflowValidation(boolean valid, RDFData explanation, List<Step> workflow) {
        super(valid, explanation);
        this.workflow = workflow;
    }

    public List<Step> getWorkflow() {
        return workflow;
    }

    public static class Step {
        private final Desire desire;
        private final DesireProvider provider;
        private final IOValidation validation;

        public Step(Desire desire, DesireProvider provider, IOValidation validation) {
            this.desire = desire;
            this.provider = provider;
            this.validation = validation;
        }

        public Desire getDesire() {
            return desire;
        }

        public DesireProvider getProvider() {
            return provider;
        }

        public IOValidation getValidation() {
            return validation;
        }

        public RDFData fulfill(RDFData inputData) throws FulfillmentException {
            return provider.fulfill(desire, inputData, validation);
        }
    }
}
