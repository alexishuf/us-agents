package br.ufsc.lapesd.us.agents.exception;

import br.ufsc.lapesd.us.agents.desires.DesireExecutor;

/**
 * Wraps exceptions thrown by {@link DesireExecutor} instances.
 */
public class ExecutorInvocationException extends FulfillmentException {
    public ExecutorInvocationException(Exception cause) {
        super("Executor thrown " + cause.getClass().getName(), cause);
    }
}
