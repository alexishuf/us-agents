package br.ufsc.lapesd.us.agents;

import org.apache.commons.io.output.ByteArrayOutputStream;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFLanguages;

import java.io.ByteArrayInputStream;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.util.function.Consumer;

/**
 * A wrapper for serializable RDF data exchanged between the agents.
 */
public class RDFData implements Serializable {
    private byte data[];
    private String contentType;

    public RDFData(byte[] data, String contentType) {
        this.data = data;
        this.contentType = contentType;
    }

    public byte[] getData() {
        return data;
    }

    public String getContentType() {
        return contentType;
    }

    public Model createModel() {
        Model model = ModelFactory.createDefaultModel();
        Lang lang = RDFLanguages.contentTypeToLang(contentType);
        ByteArrayInputStream input = new ByteArrayInputStream(data);
        RDFDataMgr.read(model, input, lang);
        return model;
    }

    @SafeVarargs
    public final void with(Consumer<Model>... consumers) {
        Model model = createModel();
        for (Consumer<Model> consumer : consumers) consumer.accept(model);
        model.close();
    }

    public static RDFData fromModel(Model model, Lang lang) {
        ByteArrayOutputStream out = new ByteArrayOutputStream();
        RDFDataMgr.write(out, model, lang);
        return new RDFData(out.toByteArray(), lang.getContentType().getContentType());
    }

    public static RDFData fromModel(Model model, String contentTypeOrLang) {
        /* nameToLang() calls shortNameToLang and contentTypeToLang() */
        return fromModel(model, RDFLanguages.nameToLang(contentTypeOrLang));
    }

    public static RDFData toRDF(Model model, Lang lang) {
        RDFData data = fromModel(model, lang);
        model.close();
        return data;
    }

    public static RDFData toRDF(Model model, String contentTypeOrLang) {
        RDFData data = fromModel(model, contentTypeOrLang);
        model.close();
        return data;
    }

    public static RDFData toTurtle(Model model) {
        RDFData data = fromModel(model, Lang.TURTLE);
        model.close();
        return data;
    }

    public static RDFData fromTurtle(String turtleInput) {
        try {
            return new RDFData(turtleInput.getBytes("UTF-8"), Lang.TURTLE.getContentType().getContentType());
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("Unexpected exception");
        }
    }
}
