package br.ufsc.lapesd.us.agents.desires.rdf;

import br.ufsc.lapesd.us.agents.desires.InformationElement;

public class RDFInformationElement implements InformationElement {
    private final String iri;

    public RDFInformationElement(String iri) {
        this.iri = iri;
    }

    public String getIRI() {
        return iri;
    }

    @Override
    public boolean equals(Object other) {
        try {
            RDFInformationElement otherRDF = (RDFInformationElement) other;
            return iri.equals(otherRDF.iri);
        } catch (ClassCastException e) {
            return false;
        }
    }

    @Override
    public int hashCode() {
        return iri.hashCode();
    }

    @Override
    public String toString() {
        return String.format("RDFInformationElement(%1$s)", getIRI());
    }
}
