package br.ufsc.lapesd.us.agents.exception;

import br.ufsc.lapesd.us.agents.RDFData;

/**
 * Some input data that is required is not present.
 */
public class MissingInputsException extends FulfillmentException {
    private RDFData requiredInputs;

    public MissingInputsException(RDFData requiredInputs) {
        this.requiredInputs = requiredInputs;
    }
}
