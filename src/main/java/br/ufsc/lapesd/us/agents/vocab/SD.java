package br.ufsc.lapesd.us.agents.vocab;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class SD {
    public static final String IRI = "http://alexishuf.bitbucket.org/2016/07/us-agents/sd.ttl";
    public static final String PREFIX = IRI + "#";

    public static final Resource ServiceDescription = ResourceFactory.createResource(PREFIX + "ServiceDescription");
    public static final Property inputConstraints = ResourceFactory.createProperty(PREFIX + "inputConstraints");
    public static final Property outputConstraints = ResourceFactory.createProperty(PREFIX + "outputConstraints");
}
