package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.*;
import br.ufsc.lapesd.us.agents.exception.ExecutorInvocationException;
import br.ufsc.lapesd.us.agents.ioconstraints.DesireExecutorIOConstraints;
import br.ufsc.lapesd.us.agents.ioconstraints.IOConstraints;
import br.ufsc.lapesd.us.agents.priv.RelationHelper;
import br.ufsc.lapesd.us.agents.exception.FulfillmentException;
import br.ufsc.lapesd.us.agents.exception.MissingInputsException;
import br.ufsc.lapesd.us.agents.ioconstraints.IOValidation;
import br.ufsc.lapesd.us.agents.ioconstraints.SimpleIOValidation;
import org.apache.commons.lang3.tuple.ImmutablePair;
import org.apache.jena.ext.com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Implementation of all functionality provided by a Service Agent. This class is not an agent.
 */
public class ServiceAgentImpl implements DesireProvider {
    private final String consolidatedOntology;
    private final String entryPoint;
    private List<ImmutablePair<DesireExecutor, ParticipationRules>> executors = null;
    private long load = 0;
    private RelationHelper relationHelper;
    private HashMap<DesireExecutor, IOConstraints> validationHelpers = null;

    public ServiceAgentImpl(String consolidatedOntology, String entryPoint) {
        this.consolidatedOntology = consolidatedOntology;
        this.entryPoint = entryPoint;
        this.relationHelper = new RelationHelper(this);

        final ParticipationRulesFactory fac = ParticipationRulesFactory.getInstance();

        List<DesireExecutor> executors;
        executors = DesireExtractor.getInstance().createExecutors(this.entryPoint);
        List<ImmutablePair<DesireExecutor, ParticipationRules>> pairs;
        pairs = new ArrayList<>(executors.size());

        for (DesireExecutor ex : executors) {
            ParticipationRules rules = fac.create(ex.getDesire(), this.consolidatedOntology);
            pairs.add(ImmutablePair.of(ex, rules));
        }

        this.executors = pairs;
        setupValidationHelpers();
    }

    private void setupValidationHelpers() {
        this.validationHelpers = new HashMap<>();
        for (ImmutablePair<DesireExecutor, ParticipationRules> pair : this.executors)
            validationHelpers.put(pair.left, new DesireExecutorIOConstraints(pair.left));
    }

    public String getConsolidatedOntology() {
        return consolidatedOntology;
    }

    public String getEntryPoint() {
        return entryPoint;
    }

    @Override
    public String getName() {
        return String.format("ServiceAgent(%1$s)", entryPoint);
    }

    @Override
    public RDFData fulfill(@Nonnull Desire desire, @Nonnull RDFData inputData,
                           @Nullable IOValidation validation) throws FulfillmentException {
        Preconditions.checkState(executors != null);
        incLoad();
        try {
            ImmutablePair<DesireExecutor, Participation> pair = getBestExecutor(desire);
            Preconditions.checkArgument(!pair.right.isUnrelated(),
                    "Agent has no participation in this desire");

            if (validation == null)
                validation = validationHelpers.get(pair.left).validateInput(desire, inputData);
            if (!validation.isValid())
                throw new MissingInputsException(validation.getExplanation());

            Model outputModel = ModelFactory.createDefaultModel();
            Model inputModel = inputData.createModel();
            pair.left.execute(inputModel, outputModel);
            inputModel.close();
            return RDFData.toTurtle(outputModel);
        } catch (FulfillmentException e) {
            throw e; //avoids the next block to catch it
        } catch (Exception e) {
            throw new ExecutorInvocationException(e);
        } finally {
            decLoad();
        }
    }

    @Override
    public Participation queryParticipation(@Nonnull Desire desire) {
        ImmutablePair<DesireExecutor, Participation> pair = getBestExecutor(desire);
        return pair.right;
    }

    @Override
    public ParticipationContainment relationWith(@Nonnull DesireProvider other,
                                                 @Nonnull Desire desire) {
        ImmutablePair<DesireExecutor, Participation> best = getBestExecutor(desire);
        Desire providedDesire = best.left != null ? best.left.getDesire() : null;
        return relationHelper.relationWith(providedDesire, best.right, other, desire);
    }

    @Override
    public @Nonnull IOValidation validateInput(@Nonnull Desire desire, RDFData inputData) {
        ImmutablePair<DesireExecutor, Participation> best = getBestExecutor(desire);
        if (best.left == null) {
            RDFData explanation = RDFData.toTurtle(ModelFactory.createDefaultModel());
            return new SimpleIOValidation(false, explanation);
        }
        return validationHelpers.get(best.left).validateInput(desire, inputData);
    }

    @Override
    public RDFData exampleOutput(@Nonnull Desire myDesire) {
        ImmutablePair<DesireExecutor, Participation> best = getBestExecutor(myDesire);
        if (best.left != null)
            return validationHelpers.get(best.left).exampleOutput(myDesire);
        else
            return RDFData.toTurtle(ModelFactory.createDefaultModel());
    }

    private ImmutablePair<DesireExecutor, Participation> getBestExecutor(Desire desire) {
        long load = this.load;
        ImmutablePair<DesireExecutor, Participation> best;
        best = ImmutablePair.of(null, new Participation(ParticipationType.UNRELATED, load));
        for (ImmutablePair<DesireExecutor, ParticipationRules> pair : executors) {
            Participation participation = pair.right.queryParticipation(desire, load);
            if (participation.betterThan(best.right))
                best = ImmutablePair.of(pair.left, participation);
        }
        return best;
    }

    protected synchronized long incLoad() {
        return load++;
    }

    protected synchronized long decLoad() {
        return load--;
    }
}
