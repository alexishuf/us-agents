package br.ufsc.lapesd.us.agents.vocab;


import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class USA {
    public static String IRI = "http://alexishuf.bitbucket.org/2016/07/us-agents/usa.ttl";
    public static String PREFIX = IRI + "#";

    public static Resource Desire = ResourceFactory.createResource(PREFIX + "Desire");
    public static Resource CompositeDesire = ResourceFactory.createResource(PREFIX + "CompositeDesire");
    public static Resource InformationElement = ResourceFactory.createResource(PREFIX + "InformationElement");

    public static Property informationElement = ResourceFactory.createProperty(PREFIX + "informationElement");
    public static Property relatedDesire = ResourceFactory.createProperty(PREFIX + "relatedDesire");
}
