package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.Participation;
import br.ufsc.lapesd.us.agents.desires.ParticipationContainment;
import br.ufsc.lapesd.us.agents.desires.ParticipationType;
import br.ufsc.lapesd.us.agents.desires.Desire;

import java.util.*;
import java.util.stream.Collectors;

/**
 * Default implementation of {@link ProviderSelector}
 */
public class DefaultProviderSelector implements ProviderSelector {
    private LinkedHashSet<DesireProvider> providers = new LinkedHashSet<>();

    static class EvaluatedDesireProvider implements Comparable<EvaluatedDesireProvider> {
        public final DesireProvider provider;
        public final Participation participation;

        public EvaluatedDesireProvider(DesireProvider provider, Participation participation) {
            this.provider = provider;
            this.participation = participation;
        }

        @Override
        public int compareTo(EvaluatedDesireProvider other) {
            if (participation.betterThan(other.participation)) return -1;
            else if (!participation.equals(other.participation)) return 1;
            return Integer.compare(hashCode(), other.hashCode());
        }

        @Override
        public boolean equals(Object o) {
            return this == o;
        }

        @Override
        public int hashCode() {
            return System.identityHashCode(this);
        }
    }


    @Override
    public void addProvider(DesireProvider provider) {
        providers.add(provider);
    }
    @Override
    public void removeProvider(DesireProvider provider) {
        providers.remove(provider);
    }

    @Override
    public List<DesireProvider> select(Desire desire) {
        TreeSet<EvaluatedDesireProvider> ordered = providers.stream()
                .map(p -> new EvaluatedDesireProvider(p, p.queryParticipation(desire)))
                .filter(e -> !e.participation.isUnrelated())
                .collect(Collectors.toCollection(TreeSet::new));
        if (ordered.isEmpty()) return Collections.emptyList();


        ArrayList<EvaluatedDesireProvider> partition = new ArrayList<>(ordered.size());
        ParticipationType best = ordered.iterator().next().participation.getParticipationType();
        for (EvaluatedDesireProvider e : ordered) {
            if (best.compareTo(e.participation.getParticipationType()) < 0) break;
            partition.add(e);
            if (e.participation.isComplete()) break;
        }

        if (partition.size() > 1)
            partition = eliminateCompetition(partition, desire);
        return partition.stream().map(e -> e.provider).collect(Collectors.toList());
    }

    private ArrayList<EvaluatedDesireProvider>
    eliminateCompetition(List<EvaluatedDesireProvider> list, Desire desire) {
        ArrayList<EvaluatedDesireProvider> array = new ArrayList<>(list.size());
        array.addAll(list);

        for (int i = 0; i < array.size(); i++) {
            if (array.get(i) == null) continue;
            for (int j = i+1; j < array.size(); j++) {
                if (array.get(j) == null) continue;
                ParticipationContainment rel;
                rel = array.get(i).provider.relationWith(array.get(j).provider, desire);


                if (rel.equals(ParticipationContainment.CONTAINS)
                        || rel.equals(ParticipationContainment.EQUIVALENT)) {
                    /* j's functionality is covered by i */
                    array.set(j, null);
                } else if (rel.equals(ParticipationContainment.CONTAINED)) {
                    /* i's functionality is covered j */
                    array.set(i, null);
                    break;
                }
            }
        }
        return array.stream().filter(e -> e != null)
                .collect(Collectors.toCollection(ArrayList::new));
    }

}
