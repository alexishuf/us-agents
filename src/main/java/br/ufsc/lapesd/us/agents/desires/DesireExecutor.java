package br.ufsc.lapesd.us.agents.desires;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;

/**
 * Executes a desire. Usually by directly interacting with a service.
 */
public interface DesireExecutor {
    /**
     * Desire fulfilled by the executor
     * @return
     */
    Desire getDesire();

    /**
     * Returns the description of the service provided by this executor.
     *
     * If there is no description, null is returned.
     *
     * @return instance of http://alexishuf.bitbucket.org/2016/07/us-agents/sd.ttl#ServiceDescription
     */
    Resource getServiceDescription();

    void execute(Model inputData, Model output) throws Exception;
}
