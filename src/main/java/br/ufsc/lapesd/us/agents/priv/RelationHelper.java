package br.ufsc.lapesd.us.agents.priv;

import br.ufsc.lapesd.us.agents.DesireProvider;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.Participation;
import br.ufsc.lapesd.us.agents.desires.ParticipationContainment;
import org.apache.commons.lang3.builder.HashCodeBuilder;

import java.util.HashSet;

public class RelationHelper {

    private static class Call {
        public final Desire provided;
        public final Participation participation;
        public final DesireProvider provider;
        public final Desire target;

        private Call(Desire provided, Participation participation,
                     DesireProvider provider, Desire target) {
            this.provided = provided;
            this.participation = participation;
            this.provider = provider;
            this.target = target;
        }

        @Override
        public boolean equals(Object o) {
            if (!(o instanceof Call))
                return false;
            Call rhs = (Call)o;
            return provided.equals(rhs.provided) && participation.equals(rhs.participation)
                    && provider.equals(rhs.provider)
                    && target.equals(rhs.target);
        }

        @Override
        public int hashCode() {
            return new HashCodeBuilder().append(provided).append(participation).append(provider)
                    .append(target).hashCode();
        }
    }

    private final DesireProvider owner;
    private HashSet<Call> technicalDetour = new HashSet<>();

    public RelationHelper(DesireProvider owner) {
        this.owner = owner;
    }

    public ParticipationContainment relationWith(Desire provided,
                                                 Participation participation,
                                                 DesireProvider other,
                                                 Desire targetDesire) {
        Call call = new Call(provided, participation, other, targetDesire);
        try {
            if (technicalDetour.contains(call)) return ParticipationContainment.IRRELEVANT;
            technicalDetour.add(call);

            if (participation.isUnrelated()) return ParticipationContainment.IRRELEVANT;

            Participation otherP = other.queryParticipation(provided);
            if (otherP.isComplete())
                return ParticipationContainment.EQUIVALENT;
            else if (otherP.isPartial())
                return ParticipationContainment.CONTAINS;
            //trick: there is no "superclass" ParticipationType value. to evaluate if
            //this provider is CONTAINED, we check if from the perspective of other,
            //other contains this.
            if (other.relationWith(owner, targetDesire) == ParticipationContainment.CONTAINS)
                return ParticipationContainment.CONTAINED;

            //unrelated for all effects
            return ParticipationContainment.IRRELEVANT;
        } finally {
            technicalDetour.remove(call);
        }
    }

}
