package br.ufsc.lapesd.us.agents.desires.rdf;

import br.ufsc.lapesd.us.agents.desires.*;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.*;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.vocabulary.RDFS;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;
import java.util.Stack;

/**
 * Set of rules built from the ontology that allow a agent to quickly determine the participation
 * of an agent in fulfilling a desire.
 */
public class RDFParticipationRules implements ParticipationRules {
    private Set<InformationElement> superClasses;
    private Desire provided;

    protected RDFParticipationRules(Desire providedDesire, Set<InformationElement> superClasses) {
        this.provided = providedDesire;
        this.superClasses =  superClasses;
    }

    public static RDFParticipationRules build(Desire providedDesire,
                                              Collection<InformationElement> superClasses) {
        Set<InformationElement> set = new HashSet<>();
        set.addAll(set);
        return new RDFParticipationRules(providedDesire, set);
    }


    public static ParticipationRules build(Desire providedDesire, String consolidatedOntology) {
        InformationElement infE = providedDesire.informationElement();
        Preconditions.checkArgument(infE instanceof RDFInformationElement);
        RDFInformationElement e = (RDFInformationElement) infE;
        Model model = ModelFactory.createDefaultModel();
        RDFDataMgr.read(model, consolidatedOntology, Lang.TURTLE); //FIXME hard-coded syntax
        HashSet<InformationElement> set = new HashSet<>();

        Stack<Resource> stack = new Stack<>();
        stack.push(model.createResource(e.getIRI()));
        while (!stack.isEmpty()) {
            NodeIterator it = model.listObjectsOfProperty(stack.pop(), RDFS.subClassOf);
            while (it.hasNext()) {
                RDFNode node = it.next();
                if (!node.isResource()) continue;
                Resource resource = node.asResource();
                stack.push(resource);
                set.add(new RDFInformationElement(resource.toString()));
            }
        }

        return new RDFParticipationRules(providedDesire, set);
    }

    public Participation queryParticipation(Desire desire, long agentLoad) {
        if (provided.equivalentTo(desire))
            return new Participation(ParticipationType.COMPLETE, agentLoad);
        else if (provided.sameDesire(desire) && superClasses.contains(desire.informationElement()))
            return new Participation(ParticipationType.PARTIAL, agentLoad);
        else
            return new Participation(ParticipationType.UNRELATED, agentLoad);
    }
}
