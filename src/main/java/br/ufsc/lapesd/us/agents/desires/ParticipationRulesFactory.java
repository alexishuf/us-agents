package br.ufsc.lapesd.us.agents.desires;

import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFParticipationRules;

import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.function.BiFunction;

/**
 * Singleton for building {@link ParticipationRules} instances
 */
public class ParticipationRulesFactory {
    private static final ParticipationRulesFactory instance = new ParticipationRulesFactory();
    private HashMap<Class<? extends InformationElement>,
            BiFunction<Desire, String, ParticipationRules>> delegates = new HashMap<>();

    protected ParticipationRulesFactory() {
        delegates.put(RDFInformationElement.class, RDFParticipationRules::build);
    }

    public static ParticipationRulesFactory getInstance() {
        return instance;
    }

    public ParticipationRules create(Desire providedDesire,
                                     String consolidatedOntology) {
        Class<? extends InformationElement> aClass;
        aClass = providedDesire.informationElement().getClass();
        return delegates.entrySet().stream().filter(p -> p.getKey().isAssignableFrom(aClass))
                .map(Map.Entry::getValue).findFirst().orElseThrow(NoSuchElementException::new)
                .apply(providedDesire, consolidatedOntology);
    }

}
