package br.ufsc.lapesd.us.agents.desires;

/**
 * Allows efficient participation evaluation by using pre-computed rules.
 */
public interface ParticipationRules {
    Participation queryParticipation(Desire desire, long agentLoad);
}
