package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.Participation;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.ParticipationContainment;
import br.ufsc.lapesd.us.agents.exception.FulfillmentException;
import br.ufsc.lapesd.us.agents.ioconstraints.IOConstraints;
import br.ufsc.lapesd.us.agents.ioconstraints.IOValidation;

import javax.annotation.Nonnull;
import javax.annotation.Nullable;

public interface DesireProvider extends IOConstraints {
    String getName();

    /**
     * Fulfill a desire using the inputs available in the inputData RDF model.
     *
     * Note that inputData will not be closed.
     *
     * @param desire The desire to be fulfilled
     * @param inputData All data that the client provided in order to fulfill
     *                  the desire. The providing agent will use resources and
     *                  literals in this model as the actual inputs to services
     *                  participating in the workflow that fulfills this desire.
     * @param validation previous validation object obtained from a previous validateInput()
     *                   call with the same inputData.
     * @return RDFData with the output
     * @throws FulfillmentException if the desire could not be fulfilled by this provider
     *
     * TODO serialization
     */
    RDFData fulfill(@Nonnull Desire desire, @Nonnull RDFData inputData,
                    @Nullable IOValidation validation) throws FulfillmentException;

    default RDFData fulfill(@Nonnull Desire desire, @Nonnull RDFData inputData) throws FulfillmentException {
        return fulfill(desire, inputData, null);
    }

    /**
     * What is the participation of this desire provider in fulfilling the given desire?
     * @param desire The desire to be fulfilled.
     */
    Participation queryParticipation(@Nonnull Desire desire);

    /**
     * Evaluates {@link ParticipationContainment} considering this provider as the right-handed
     * one and other as the left-handed.
     *
     * @param other provider to compare against
     * @param desire user desire.
     * @return containment relation
     */
    ParticipationContainment relationWith(@Nonnull DesireProvider other, @Nonnull Desire desire);

}
