package br.ufsc.lapesd.us.agents.desires;

public enum ParticipationType {
    /**
     * The agent has nothing to collaborate in directly fulfilling the target desire.
     */
    UNRELATED,
    /**
     * The agent provides the desire completly, no partial solutions are needed.
     */
    COMPLETE,
    /**
     * The agent provides functionality that partially satisfies the desire.
     */
    PARTIAL
}
