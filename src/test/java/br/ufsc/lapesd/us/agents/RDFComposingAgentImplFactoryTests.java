package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.rdf.RDFComposingAgentImplFactory;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFDesire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import br.ufsc.lapesd.us.agents.exception.NoProviderException;
import br.ufsc.lapesd.us.agents.test_helpers.PoliceDesires;
import br.ufsc.lapesd.us.agents.test_helpers.Police;
import br.ufsc.lapesd.us.agents.test_helpers.PoliceScenario;
import br.ufsc.lapesd.us.agents.vocab.USA;
import br.ufsc.lapesd.us.agents.desires.Desire;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.IOException;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Test
public class RDFComposingAgentImplFactoryTests {
    private static RDFDesire getInf(String element) {
        return new RDFDesire(PoliceDesires.GetInformation, new RDFInformationElement(element));
    }

    public class OcorrenciaPolicial extends PoliceScenario {
        private Resource resource;

        public OcorrenciaPolicial() throws Exception {
        }

        @BeforeMethod
        @Override
        public void init() throws IOException {
            super.init();
            Model model = ModelFactory.createDefaultModel();
            model.read(getOntologyUrl(), null, "TTL");
            resource = model.listSubjectsWithProperty(USA.informationElement,
                    ResourceFactory.createResource(Police.PoliceOccurrence)).next();


        }

        @Test
        public void testCreationFailDueToNoProvider() {
            boolean caught = false;
            ComposingAgentImpl agent = null;
            try {
                agent = RDFComposingAgentImplFactory.getInstance().createAgentImpl(resource,
                        getOntologyUrl(), new DefaultProviderSelector());
            } catch (NoProviderException e) {
                caught = true;
            }
            Assert.assertTrue(caught);
        }

        @Test
        public void testRelated() {
            Set<Desire> actual = RDFComposingAgentImplFactory.getInstance()
                    .getRelatedDesires(resource).stream().collect(Collectors.toSet());
            Set<RDFDesire> expected = Stream.of(getInf(Police.PoliceReport),
                    getInf(Police.InvolvedParty), getInf(Police.Location))
                    .collect(Collectors.toSet());
            Assert.assertEquals(actual, expected);
        }
    }
}
