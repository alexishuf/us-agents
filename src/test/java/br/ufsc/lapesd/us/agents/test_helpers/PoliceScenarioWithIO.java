package br.ufsc.lapesd.us.agents.test_helpers;

import br.ufsc.lapesd.us.agents.desires.DesireExecutor;
import br.ufsc.lapesd.us.agents.desires.DesireExtractor;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.ResourceFactory;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Helper class that registers {@link DesireExecutor} instances
 * with IO constraint descriptions on {@link DesireExtractor}.
 */
public class PoliceScenarioWithIO extends PoliceScenario {

    private MockExecutor mock(String entryPoint, String desire, String element,
                              String descriptionTTL, String descriptionIri) {
        MockExecutor ex = new MockExecutor(entryPoint, desire, element);

        Model model = ModelFactory.createDefaultModel();
        model.read(IOUtils.toInputStream(descriptionTTL), null, "TTL");
        assert  model.containsResource(ResourceFactory.createResource(descriptionIri));
        ex.setDescription(model, model.createResource(descriptionIri));
        return ex;
    }

    private String getTTL(String entryPoint) {
        ClassLoader loader = getClass().getClassLoader();
        Matcher matcher = Pattern.compile("http://([^\\.]+)\\..*").matcher(entryPoint);
        assert matcher.matches();
        String name = matcher.group(1);
        InputStream stream = loader.getResourceAsStream("police-descriptions/" + name + ".ttl");
        try {
            return IOUtils.toString(stream);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void init() throws IOException {
        super.init("police/desires.ttl", "police/ontology.ttl", "police/composite-desires.ttl");
        addExecutorSupplier(homicideService, () -> Arrays.asList(
                mock(homicideService, PoliceDesires.GetInformation, Police.MurderReport, getTTL(homicideService), homicideService + "/description#service"),
                mock(homicideService, PoliceDesires.Register, Police.MurderReport, getTTL(homicideService), homicideService + "/description#service")
        ));
        addExecutorSupplier(latrocinioService, () -> Arrays.asList(
                mock(latrocinioService, PoliceDesires.GetInformation, Police.RobberyReport, getTTL(latrocinioService), latrocinioService + "/description#service"),
                mock(latrocinioService, PoliceDesires.Register, Police.RobberyReport, getTTL(latrocinioService), latrocinioService + "/description#service")
        ));
        addExecutorSupplier(suspiciousdeathService, () -> Arrays.asList(
                mock(suspiciousdeathService, PoliceDesires.GetInformation, Police.SuicideReport, getTTL(suspiciousdeathService), suspiciousdeathService + "/description#service"),
                mock(suspiciousdeathService, PoliceDesires.Register, Police.SuicideReport, getTTL(suspiciousdeathService), suspiciousdeathService + "/description#service")
        ));
        addExecutorSupplier(victimService, () -> Arrays.asList(
                mock(victimService, PoliceDesires.GetInformation, Police.Victim, getTTL(victimService), victimService + "/description#service"),
                mock(victimService, PoliceDesires.Register, Police.Victim, getTTL(victimService), victimService + "/description#service")
        ));
        addExecutorSupplier(authorService, () -> Arrays.asList(
                mock(authorService, PoliceDesires.GetInformation, Police.Author, getTTL(authorService), authorService + "/description#service"),
                mock(authorService, PoliceDesires.Register, Police.Author, getTTL(authorService), authorService + "/description#service")
        ));
        addExecutorSupplier(locationService, () -> Arrays.asList(
                mock(locationService, PoliceDesires.GetInformation, Police.Location, getTTL(locationService), locationService + "/description#service"),
                mock(locationService, PoliceDesires.Register, Police.Location, getTTL(locationService), locationService + "/description#service")
        ));
        addExecutorSupplier(stationService, () -> Collections.singletonList(
                mock(stationService, PoliceDesires.GetInformation, Police.PoliceStation, getTTL(stationService), stationService + "/description#service")
        ));
    }
}
