package br.ufsc.lapesd.us.agents.test_helpers;

import br.ufsc.lapesd.us.agents.ProviderSelector;
import br.ufsc.lapesd.us.agents.ServiceAgentImpl;
import br.ufsc.lapesd.us.agents.desires.DesireExecutor;
import br.ufsc.lapesd.us.agents.desires.DesireExtractor;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;

/**
 * Helper class that registers forced {@link DesireExecutor}
 * instances on {@link DesireExtractor}.
 */
public abstract class PoliceScenarioNoIO extends AbstractScenario {
    public static final String homicideService = "http://homicide.seguranca.br/api";
    public static final String latrocinioService = "http://latrocinio.seguranca.br/api";
    public static final String suspiciousdeathService = "http://suspiciousdeath.seguranca.br/api";
    public static final String victimService = "http://victim.seguranca.br/api";
    public static final String authorService = "http://author.seguranca.br/api";
    public static final String locationService = "http://location.seguranca.br/api";
    public static final String stationService = "http://station.seguranca.br/api";


    public void createServiceAgents(ProviderSelector selector) {
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), homicideService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), latrocinioService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), suspiciousdeathService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), victimService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), authorService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), locationService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), stationService));
    }

    @BeforeMethod
    @Override
    public void init() throws IOException {
        super.init("police/desires.ttl", "police/ontology.ttl", "police/composite-desires.ttl");
        addExecutorSupplier(homicideService, () -> Arrays.asList(
                new MockExecutor(homicideService, PoliceDesires.GetInformation, Police.MurderReport),
                new MockExecutor(homicideService, PoliceDesires.Register, Police.MurderReport)
        ));
        addExecutorSupplier(latrocinioService, () -> Arrays.asList(
                new MockExecutor(latrocinioService, PoliceDesires.GetInformation, Police.RobberyReport),
                new MockExecutor(latrocinioService, PoliceDesires.Register, Police.RobberyReport)
        ));
        addExecutorSupplier(suspiciousdeathService, () -> Arrays.asList(
                new MockExecutor(suspiciousdeathService, PoliceDesires.GetInformation, Police.SuicideReport),
                new MockExecutor(suspiciousdeathService, PoliceDesires.Register, Police.SuicideReport)
        ));
        addExecutorSupplier(victimService, () -> Arrays.asList(
                new MockExecutor(victimService, PoliceDesires.GetInformation, Police.Victim),
                new MockExecutor(victimService, PoliceDesires.Register, Police.Victim)
        ));
        addExecutorSupplier(authorService, () -> Arrays.asList(
                new MockExecutor(authorService, PoliceDesires.GetInformation, Police.Author),
                new MockExecutor(authorService, PoliceDesires.Register, Police.Author)
        ));
        addExecutorSupplier(locationService, () -> Arrays.asList(
                new MockExecutor(locationService, PoliceDesires.GetInformation, Police.Location),
                new MockExecutor(locationService, PoliceDesires.Register, Police.Location)
        ));
        addExecutorSupplier(stationService, () -> Collections.singletonList(
                new MockExecutor(stationService, PoliceDesires.GetInformation, Police.PoliceStation)
        ));
    }

}
