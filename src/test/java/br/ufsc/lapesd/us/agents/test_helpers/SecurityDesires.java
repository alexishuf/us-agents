package br.ufsc.lapesd.us.agents.test_helpers;

import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class SecurityDesires {
    public static final String PREFIX = "http://alexishuf.bitbucket.org/2016/07/us-agents/examples/security/desires.ttl#";

    public static final String Blacklist = PREFIX + "Blacklist";
    public static final String FreezeAssets = PREFIX + "FreezeAssets";
    public static final String GetInformation = PREFIX + "GetInformation";
    public static final String Register = PREFIX + "Register";
    public static final String NotifyCrime = PREFIX + "NotifyCrime";

    public static class R {
        public static final Resource Blacklist = ResourceFactory.createResource(SecurityDesires.Blacklist);
        public static final Resource FreezeAssets = ResourceFactory.createResource(SecurityDesires.FreezeAssets);
        public static final Resource GetInformation = ResourceFactory.createResource(SecurityDesires.GetInformation);
        public static final Resource Register = ResourceFactory.createResource(SecurityDesires.Register);
        public static final Resource NotifyCrime = ResourceFactory.createResource(SecurityDesires.NotifyCrime);
    }
}
