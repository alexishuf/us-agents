package br.ufsc.lapesd.us.agents.test_helpers;

public class CrudDesires {
    public static final String PREFIX = "http://example.org/crud-desires.ttl#";

    public static final String Create = PREFIX + "Create";
    public static final String Retrieve = PREFIX + "Retrieve";
    public static final String Update = PREFIX + "Update";
    public static final String Delete = PREFIX + "Delete";
}
