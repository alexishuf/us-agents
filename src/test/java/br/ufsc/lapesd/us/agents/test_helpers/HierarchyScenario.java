package br.ufsc.lapesd.us.agents.test_helpers;

import br.ufsc.lapesd.us.agents.ProviderSelector;
import br.ufsc.lapesd.us.agents.ServiceAgentImpl;
import br.ufsc.lapesd.us.agents.desires.DesireExtractor;
import br.ufsc.lapesd.us.agents.exception.NoProviderException;
import org.testng.annotations.BeforeMethod;

import java.io.IOException;
import java.util.Arrays;

public abstract class HierarchyScenario extends AbstractScenario {
    public static final String aService = "http://a.example.org/";
    public static final String aaService = "http://aa.example.org/";
    public static final String aaaService = "http://aaa.example.org/";
    public static final String aaaaService = "http://aaaa.example.org/";
    public static final String aaabService = "http://aaab.example.org/";
    public static final String aabService = "http://aab.example.org/";
    public static final String abService = "http://ab.example.org/";
    public static final String bService = "http://b.example.org/";
    public static final String cService = "http://c.example.org/";

    @Override
    public void createServiceAgents(ProviderSelector selector) {
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), aService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), aaService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), aaaService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), aaaaService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), aaabService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), aabService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), abService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), bService));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), cService));
    }

    @Override
    public void createComposingAgents(ProviderSelector selector) throws NoProviderException {
        /* no-op */
    }

    @BeforeMethod
    @Override
    public void init() throws IOException {
        super.init("hierarchy-ontology.ttl", "crud-desires.ttl");
        DesireExtractor ex = DesireExtractor.getInstance();
        addExecutorSupplier(aService, () -> Arrays.asList(
                new MockExecutor(aService, CrudDesires.Create, Hierarchy.A),
                new MockExecutor(aService, CrudDesires.Retrieve, Hierarchy.A),
                new MockExecutor(aService, CrudDesires.Update, Hierarchy.A),
                new MockExecutor(aService, CrudDesires.Delete, Hierarchy.A)
        ));
        addExecutorSupplier(aaService, () -> Arrays.asList(
                new MockExecutor(aaService, CrudDesires.Create, Hierarchy.AA),
                new MockExecutor(aaService, CrudDesires.Retrieve, Hierarchy.AA),
                new MockExecutor(aaService, CrudDesires.Update, Hierarchy.AA),
                new MockExecutor(aaService, CrudDesires.Delete, Hierarchy.AA)
        ));
        addExecutorSupplier(aaaService, () -> Arrays.asList(
                new MockExecutor(aaaService, CrudDesires.Create, Hierarchy.AAA),
                new MockExecutor(aaaService, CrudDesires.Retrieve, Hierarchy.AAA),
                new MockExecutor(aaaService, CrudDesires.Update, Hierarchy.AAA),
                new MockExecutor(aaaService, CrudDesires.Delete, Hierarchy.AAA)
        ));
        addExecutorSupplier(aaaaService, () -> Arrays.asList(
                new MockExecutor(aaaaService, CrudDesires.Create, Hierarchy.AAAA),
                new MockExecutor(aaaaService, CrudDesires.Retrieve, Hierarchy.AAAA),
                new MockExecutor(aaaaService, CrudDesires.Update, Hierarchy.AAAA),
                new MockExecutor(aaaaService, CrudDesires.Delete, Hierarchy.AAAA)
        ));
        addExecutorSupplier(aaabService, () -> Arrays.asList(
                new MockExecutor(aaabService, CrudDesires.Create, Hierarchy.AAAB),
                new MockExecutor(aaabService, CrudDesires.Retrieve, Hierarchy.AAAB),
                new MockExecutor(aaabService, CrudDesires.Update, Hierarchy.AAAB),
                new MockExecutor(aaabService, CrudDesires.Delete, Hierarchy.AAAB)
        ));
        addExecutorSupplier(aabService, () -> Arrays.asList(
                new MockExecutor(aabService, CrudDesires.Create, Hierarchy.AAB),
                new MockExecutor(aabService, CrudDesires.Retrieve, Hierarchy.AAB),
                new MockExecutor(aabService, CrudDesires.Update, Hierarchy.AAB),
                new MockExecutor(aabService, CrudDesires.Delete, Hierarchy.AAB)
        ));
        addExecutorSupplier(abService, () -> Arrays.asList(
                new MockExecutor(abService, CrudDesires.Create, Hierarchy.AB),
                new MockExecutor(abService, CrudDesires.Retrieve, Hierarchy.AB),
                new MockExecutor(abService, CrudDesires.Update, Hierarchy.AB),
                new MockExecutor(abService, CrudDesires.Delete, Hierarchy.AB)
        ));
        addExecutorSupplier(bService, () -> Arrays.asList(
                new MockExecutor(bService, CrudDesires.Create, Hierarchy.B),
                new MockExecutor(bService, CrudDesires.Retrieve, Hierarchy.B),
                new MockExecutor(bService, CrudDesires.Update, Hierarchy.B),
                new MockExecutor(bService, CrudDesires.Delete, Hierarchy.B)
        ));
        addExecutorSupplier(cService, () -> Arrays.asList(
                new MockExecutor(cService, CrudDesires.Create, Hierarchy.C),
                new MockExecutor(cService, CrudDesires.Retrieve, Hierarchy.C),
                new MockExecutor(cService, CrudDesires.Update, Hierarchy.C),
                new MockExecutor(cService, CrudDesires.Delete, Hierarchy.C)
        ));
    }
}
