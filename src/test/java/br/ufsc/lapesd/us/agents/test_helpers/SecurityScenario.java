package br.ufsc.lapesd.us.agents.test_helpers;

import br.ufsc.lapesd.us.agents.ComposingAgentImpl;
import br.ufsc.lapesd.us.agents.ProviderSelector;
import br.ufsc.lapesd.us.agents.ServiceAgentImpl;
import br.ufsc.lapesd.us.agents.desires.DesireExecutor;
import br.ufsc.lapesd.us.agents.desires.DesireExtractor;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFComposingAgentImplFactory;
import br.ufsc.lapesd.us.agents.exception.NoProviderException;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

import java.io.InputStream;
import java.util.Arrays;
import java.util.List;

/**
 * Helper class that registers {@link DesireExecutor} instances on {@link DesireExtractor}.
 */
public class SecurityScenario extends AbstractScenario {
    public static final String immigrationEU = "http://immigration.eu.example.org/api";
    public static final String immigrationNA = "http://immigration.na.example.org/api";
    public static final String immigrationAS = "http://immigration.as.example.org/api";
    public static final String immigrationSA = "http://immigration.sa.example.org/api";

    public static final String financialEU = "http://financial.eu.example.org/api";
    public static final String financialNA = "http://financial.na.example.org/api";
    public static final String financialAS = "http://financial.as.example.org/api";
    public static final String financialSA = "http://financial.sa.example.org/api";

    public static final String policeFBI = "http://fbi.police.example.org/api";
    public static final String policeInterpol = "http://interpol.police.example.org/api";

    public static final String persons = "http://persons.example.org/api";


    @Override
    public void createServiceAgents(ProviderSelector selector) {
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), immigrationEU));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), immigrationNA));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), immigrationAS));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), immigrationSA));

        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), financialEU));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), financialNA));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), financialAS));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), financialSA));

        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), policeFBI));
        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), policeInterpol));

        selector.addProvider(new ServiceAgentImpl(getOntologyUrl(), persons));
    }

    @Override
    public void createComposingAgents(ProviderSelector selector) throws NoProviderException {
        createComposingAgents(selector, "usa.ttl", "security/desires.ttl", "security/composite-desires.ttl");
    }

    @Override
    protected void init() throws Exception {
        super.init("security/desires.ttl", "security/ontology.ttl");
        addExecutorSupplier(immigrationEU, () -> Arrays.asList(
                new MockExecutor(immigrationEU, SecurityDesires.GetInformation, Security.ImmigrationRecordEU),
                new MockExecutor(immigrationEU, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordEU)
        ));
        addExecutorSupplier(immigrationNA, () -> Arrays.asList(
                new MockExecutor(immigrationNA, SecurityDesires.GetInformation, Security.ImmigrationRecordNA),
                new MockExecutor(immigrationNA, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordNA)
        ));
        addExecutorSupplier(immigrationAS, () -> Arrays.asList(
                new MockExecutor(immigrationAS, SecurityDesires.GetInformation, Security.ImmigrationRecordAS),
                new MockExecutor(immigrationAS, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordAS)
        ));
        addExecutorSupplier(immigrationSA, () -> Arrays.asList(
                new MockExecutor(immigrationSA, SecurityDesires.GetInformation, Security.ImmigrationRecordSA),
                new MockExecutor(immigrationSA, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordSA)
        ));

        addExecutorSupplier(financialEU, () -> Arrays.asList(
                new MockExecutor(financialEU, SecurityDesires.GetInformation, Security.FinancialTransactionEU),
                new MockExecutor(financialEU, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionEU)
        ));
        addExecutorSupplier(financialNA, () -> Arrays.asList(
                new MockExecutor(financialNA, SecurityDesires.GetInformation, Security.FinancialTransactionNA),
                new MockExecutor(financialNA, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionNA)
        ));
        addExecutorSupplier(financialAS, () -> Arrays.asList(
                new MockExecutor(financialAS, SecurityDesires.GetInformation, Security.FinancialTransactionAS),
                new MockExecutor(financialAS, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionAS)
        ));
        addExecutorSupplier(financialSA, () -> Arrays.asList(
                new MockExecutor(financialSA, SecurityDesires.GetInformation, Security.FinancialTransactionSA),
                new MockExecutor(financialSA, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionSA)
        ));

        addExecutorSupplier(policeFBI, () -> Arrays.asList(
                new MockExecutor(policeFBI, SecurityDesires.GetInformation, Security.CriminalRecordFBI),
                new MockExecutor(policeFBI, SecurityDesires.Register, Security.CriminalRecordFBI)
        ));
        addExecutorSupplier(policeInterpol, () -> Arrays.asList(
                new MockExecutor(policeInterpol, SecurityDesires.GetInformation, Security.CriminalRecordInterpol),
                new MockExecutor(policeInterpol, SecurityDesires.Register, Security.CriminalRecordInterpol)
        ));

        addExecutorSupplier(persons, () -> Arrays.asList(
                new MockExecutor(persons, SecurityDesires.GetInformation, Security.Person),
                new MockExecutor(persons, SecurityDesires.Register, Security.Person)
        ));
    }
}
