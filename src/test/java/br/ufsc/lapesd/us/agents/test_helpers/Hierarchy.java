package br.ufsc.lapesd.us.agents.test_helpers;

public class Hierarchy {
    public static final String PREFIX = "http://example.org/hierarchy-ontology.ttl#";

    public static final String A = PREFIX + "A";
    public static final String AA = PREFIX + "AA";
    public static final String AB = PREFIX + "AB";
    public static final String AAA = PREFIX + "AAA";
    public static final String AAAA = PREFIX + "AAAA";
    public static final String AAAB = PREFIX + "AAAB";
    public static final String AAB = PREFIX + "AAB";
    public static final String B = PREFIX + "B";
    public static final String C = PREFIX + "C";
}
