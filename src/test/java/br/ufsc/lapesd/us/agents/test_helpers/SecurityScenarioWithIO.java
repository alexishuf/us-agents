package br.ufsc.lapesd.us.agents.test_helpers;

import br.ufsc.lapesd.us.agents.vocab.SD;
import br.ufsc.lapesd.us.agents.vocab.USA;
import org.apache.jena.query.QueryExecution;
import org.apache.jena.query.QueryExecutionFactory;
import org.apache.jena.query.QueryFactory;
import org.apache.jena.query.ResultSet;
import org.apache.jena.rdf.model.*;

import java.io.InputStream;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Version of {@link SecurityScenario} with IO descriptions
 */
public class SecurityScenarioWithIO  extends SecurityScenario {

    private MockExecutor mock(String entryPoint, String desire, String element) {
        MockExecutor ex = new MockExecutor(entryPoint, desire, element);

        Model model = loadDescription(entryPoint);
        Resource resource = findDescriptionResource(model, desire, element);
        ex.setDescription(model, resource);
        return ex;
    }

    private Resource findDescriptionResource(Model model, String desire, String element) {
        QueryExecution ex = QueryExecutionFactory.create(QueryFactory.create(
                "PREFIX usa: <" + USA.PREFIX + ">\n" +
                "PREFIX sd: <" + SD.PREFIX + ">\n" +
                "SELECT ?x WHERE {\n" +
                "  ?x sd:fulfills ?y .\n" +
                "  ?y a <" + desire + "> .\n" +
                "  ?y usa:informationElement <" + element + ">.\n" +
                "}"), model);
        ResultSet results = ex.execSelect();
        assert results.hasNext();
        Resource resource = results.next().get("x").asResource();
        assert !results.hasNext();
        ex.close();
        return resource;
    }

    private Model loadDescription(String entryPoint) {
        ClassLoader loader = getClass().getClassLoader();
        Matcher matcher = Pattern.compile("http://(.+)\\.example.*").matcher(entryPoint);
        boolean matches = matcher.matches();
        assert matches;
        String name = matcher.group(1);
        InputStream stream = loader.getResourceAsStream("security-descriptions/" + name + ".ttl");
        Model model = ModelFactory.createDefaultModel();
        model.read(stream, null, "TTL");
        return model;
    }

    @Override
    protected void init() throws Exception {
        super.init("security/desires.ttl", "security/ontology.ttl",
                   "security/composite-desires.ttl");
        addExecutorSupplier(immigrationEU, () -> Arrays.asList(
                mock(immigrationEU, SecurityDesires.GetInformation, Security.ImmigrationRecordEU),
                mock(immigrationEU, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordEU)
        ));
        addExecutorSupplier(immigrationNA, () -> Arrays.asList(
                mock(immigrationNA, SecurityDesires.GetInformation, Security.ImmigrationRecordNA),
                mock(immigrationNA, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordNA)
        ));
        addExecutorSupplier(immigrationAS, () -> Arrays.asList(
                mock(immigrationAS, SecurityDesires.GetInformation, Security.ImmigrationRecordAS),
                mock(immigrationAS, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordAS)
        ));
        addExecutorSupplier(immigrationSA, () -> Arrays.asList(
                mock(immigrationSA, SecurityDesires.GetInformation, Security.ImmigrationRecordSA),
                mock(immigrationSA, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordSA)
        ));

        addExecutorSupplier(financialEU, () -> Arrays.asList(
                mock(financialEU, SecurityDesires.GetInformation, Security.FinancialTransactionEU),
                mock(financialEU, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionEU)
        ));
        addExecutorSupplier(financialNA, () -> Arrays.asList(
                mock(financialNA, SecurityDesires.GetInformation, Security.FinancialTransactionNA),
                mock(financialNA, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionNA)
        ));
        addExecutorSupplier(financialAS, () -> Arrays.asList(
                mock(financialAS, SecurityDesires.GetInformation, Security.FinancialTransactionAS),
                mock(financialAS, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionAS)
        ));
        addExecutorSupplier(financialSA, () -> Arrays.asList(
                mock(financialSA, SecurityDesires.GetInformation, Security.FinancialTransactionSA),
                mock(financialSA, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionSA)
        ));

        addExecutorSupplier(policeFBI, () -> Arrays.asList(
                mock(policeFBI, SecurityDesires.GetInformation, Security.CriminalRecordFBI),
                mock(policeFBI, SecurityDesires.Register, Security.CriminalRecordFBI)
        ));
        addExecutorSupplier(policeInterpol, () -> Arrays.asList(
                mock(policeInterpol, SecurityDesires.GetInformation, Security.CriminalRecordInterpol),
                mock(policeInterpol, SecurityDesires.Register, Security.CriminalRecordInterpol)
        ));

        addExecutorSupplier(persons, () -> Arrays.asList(
                mock(persons, SecurityDesires.GetInformation, Security.Person),
                mock(persons, SecurityDesires.Register, Security.Person)
        ));
    }
}
