package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.Participation;
import br.ufsc.lapesd.us.agents.desires.ParticipationType;
import br.ufsc.lapesd.us.agents.test_helpers.*;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.ParticipationContainment;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFDesire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import br.ufsc.lapesd.us.agents.exception.FulfillmentException;
import br.ufsc.lapesd.us.agents.exception.MissingInputsException;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

@Test
public class ServiceAgentImplTests {

    private static Desire des(String desire, String element) {
        return new RDFDesire(desire, new RDFInformationElement(element));
    }

    @Test
    public class PoliceTests extends PoliceScenario {
        public PoliceTests() throws Exception {
        }

        @DataProvider(name = "participation")
        public Object[][] dataParticipation() {
            return new Object[][]{
                    {des(PoliceDesires.GetInformation, Police.MurderReport),
                            ParticipationType.COMPLETE},
                    {des(PoliceDesires.GetInformation, Police.PoliceReport),
                            ParticipationType.PARTIAL},
                    {des(PoliceDesires.Register, Police.MurderReport),
                            ParticipationType.COMPLETE},
                    {des(PoliceDesires.Register, Police.PoliceReport),
                            ParticipationType.PARTIAL},
                    {des(PoliceDesires.GetInformation, Police.Person),
                            ParticipationType.UNRELATED}
            };
        }

        @Test(dataProvider = "participation")
        public void testParticipation(Desire desire, ParticipationType expected) {
            ServiceAgentImpl ag;
            ag = new ServiceAgentImpl(getOntologyUrl(), PoliceScenario.homicideService);
            Participation actual = ag.queryParticipation(desire);
            Assert.assertEquals(actual.getParticipationType(), expected);
        }

        @DataProvider(name = "descriptions")
        public Object[][] dataDescriptions() {
            String descriptionIRI = homicideService + "/description#service";
            String prologue = "@prefix o: <http://alexishuf.bitbucket.org/2016/07/us-agents/examples/police/ontology.ttl#> .\n" +
                    "@prefix sd: <http://alexishuf.bitbucket.org/2016/07/us-agents/sd.ttl#> .\n" +
                    "@prefix sp: <http://spinrdf.org/sp#> .\n" +
                    "@prefix ex: <" + homicideService + "/description#" + ">. \n" +
                    "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
                    "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n\n";

            String requiresRG = prologue + "" +
                    "ex:service a sd:ServiceCall ;\n" +
                    "    sd:inputConstraints [\n" +
                    "        a sp:Ask ;" +
                    "        sp:text \"\"\"\n" +
                    "            ASK WHERE {\n" +
                    "                ?victim a o:Victim .\n" +
                    "                ?victim o:id ?rg .\n" +
                    "                ?rg o:idNumber ?nrg .\n" +
                    "                ?rg o:idIssuer ?emissor .\n" +
                    "            }\n" +
                    "            \"\"\"^^xsd:string\n" +
                    "    ] .\n";
            return new Object[][] {
                    {true, prologue + "" +
                            "[\n" +
                            "    a o:Victim; " +
                            "    o:id [\n" +
                            "        o:idIssuer \"SSP/SP\"^^xsd:string ;\n" +
                            "        o:idNumber \"1.234.567-8\"^^xsd:string" +
                            "    ]\n" +
                            "] .\n",
                            requiresRG, descriptionIRI},
                    {false, prologue + "" +
                            "[\n" +
                            "    a o:Victim ;\n" +
                            "    o:age \"5\"^^xsd:nonNegativeInteger \n" +
                            "] .\n",
                            requiresRG, descriptionIRI}
            };
        }

        @Test(dataProvider = "descriptions")
        public void testValidateInputs(boolean isValid, String inputTTL, String descriptionTTL,
                                       String descriptionIRI) {
            /* setup executor description */
            Model descriptionModel = ModelFactory.createDefaultModel();
            descriptionModel.read(IOUtils.toInputStream(descriptionTTL), null, "TTL");
            Resource description = descriptionModel.createResource(descriptionIRI);

            wrapExecutors(homicideService, (ex) -> ((MockExecutor)ex)
                    .setDescription(descriptionModel, description));

            RDFData input = RDFData.toTurtle(ModelFactory.createDefaultModel()
                    .read(IOUtils.toInputStream(inputTTL), null, "TTL"));

            ServiceAgentImpl ag = new ServiceAgentImpl(getOntologyUrl(), homicideService);
            boolean caught = false;
            try {
                Desire desire = des(PoliceDesires.GetInformation, Police.MurderReport);
                RDFData output = ag.fulfill(desire, input);
                Assert.assertNotNull(output);
            } catch (MissingInputsException e) {
                caught = true;
            } catch (FulfillmentException e) {
                Assert.fail("Unexpected", e);
            }

            Assert.assertEquals(!caught, isValid);
        }
    }

    @Test
    public class HierarchyTests extends HierarchyScenario {
        public HierarchyTests() throws Exception {
        }

        @DataProvider(name = "relationWith")
        public Object[][] dataRelationWith() {
            return new Object[][] {
                    {aaService, abService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.IRRELEVANT},
                    {aService, bService, des(CrudDesires.Retrieve, Hierarchy.C),
                            ParticipationContainment.IRRELEVANT},
                    {aService, bService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.IRRELEVANT},
                    {aService, bService, des(CrudDesires.Retrieve, Hierarchy.B),
                            ParticipationContainment.IRRELEVANT},

                    {aService, aService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.EQUIVALENT},
                    {aaService, aaService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.EQUIVALENT},
                    {aabService, aabService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.EQUIVALENT},

                    {aService, aaService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.CONTAINS},
                    {aaService, aaaService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.CONTAINS},
                    {aaService, aService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.CONTAINED},
                    {aaaService, aaService, des(CrudDesires.Retrieve, Hierarchy.A),
                            ParticipationContainment.CONTAINED},
            };
        }

        @Test(dataProvider = "relationWith")
        public void testRelationWith(String lhsEP, String rhsEP, Desire desire,
                                     ParticipationContainment expected) {
            ServiceAgentImpl lhs = new ServiceAgentImpl(getOntologyUrl(), lhsEP);
            ServiceAgentImpl rhs = new ServiceAgentImpl(getOntologyUrl(), rhsEP);

            ParticipationContainment actual = lhs.relationWith(rhs, desire);
            Assert.assertEquals(actual, expected);
        }
    }
}
