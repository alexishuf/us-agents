package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.rdf.RDFDesire;
import br.ufsc.lapesd.us.agents.test_helpers.PoliceDesires;
import br.ufsc.lapesd.us.agents.test_helpers.Police;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import br.ufsc.lapesd.us.agents.vocab.USA;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;
import org.testng.Assert;
import org.testng.annotations.Test;

@Test
public class RDFDesireTests {
    @Test
    public void testFromResource() {
        ClassLoader loader = getClass().getClassLoader();
        Model model = ModelFactory.createDefaultModel();
        model.read(loader.getResourceAsStream("police/desires.ttl"), null, "TTL");
        model.read(loader.getResourceAsStream("police/composite-desires.ttl"), null, "TTL");
        Resource resource = model.listSubjectsWithProperty(USA.informationElement,
                ResourceFactory.createResource(Police.PoliceOccurrence)).next();

        RDFDesire loaded = RDFDesire.fromResource(resource);
        RDFDesire expected = new RDFDesire(PoliceDesires.GetInformation,
                new RDFInformationElement(Police.PoliceOccurrence));
        Assert.assertEquals(loaded, expected);
    }
}
