package br.ufsc.lapesd.us.agents.test_helpers;

import org.apache.jena.rdf.model.Property;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.rdf.model.ResourceFactory;

public class Security {
    public static final String PREFIX = "http://alexishuf.bitbucket.org/2016/07/us-agents/examples/security/ontology.ttl#";

    /* classes */
    public static final String CriminalRecord = PREFIX + "CriminalRecord";
    public static final String FinancialTransaction = PREFIX + "FinancialTransaction";
    public static final String ImmigrationRecord = PREFIX + "ImmigrationRecord";
    public static final String Person = PREFIX + "Person";
    public static final String ImmigrationRecordEU = PREFIX + "ImmigrationRecordEU";
    public static final String ImmigrationRecordNA = PREFIX + "ImmigrationRecordNA";
    public static final String ImmigrationRecordSA = PREFIX + "ImmigrationRecordSA";
    public static final String ImmigrationRecordAS = PREFIX + "ImmigrationRecordAS";
    public static final String FinancialTransactionEU = PREFIX + "FinancialTransactionEU";
    public static final String FinancialTransactionNA = PREFIX + "FinancialTransactionNA";
    public static final String FinancialTransactionSA = PREFIX + "FinancialTransactionSA";
    public static final String FinancialTransactionAS = PREFIX + "FinancialTransactionAS";
    public static final String CriminalRecordFBI = PREFIX + "CriminalRecordFBI";
    public static final String CriminalRecordInterpol = PREFIX + "CriminalRecordInterpol";
    public static final String PersonWithImmigrationRecordEU = PREFIX + "PersonWithImmigrationRecordEU";
    public static final String PersonWithImmigrationRecordNA = PREFIX + "PersonWithImmigrationRecordNA";
    public static final String PersonWithImmigrationRecordSA = PREFIX + "PersonWithImmigrationRecordSA";
    public static final String PersonWithImmigrationRecordAS = PREFIX + "PersonWithImmigrationRecordAS";
    public static final String PersonWithFinancialTransactionEU = PREFIX + "PersonWithFinancialTransactionEU";
    public static final String PersonWithFinancialTransactionNA = PREFIX + "PersonWithFinancialTransactionNA";
    public static final String PersonWithFinancialTransactionSA = PREFIX + "PersonWithFinancialTransactionSA";
    public static final String PersonWithFinancialTransactionAS = PREFIX + "PersonWithFinancialTransactionAS";


    /* properties */
    public static final String amount = PREFIX + "amount";
    public static final String arrivalDate = PREFIX + "arrivalDate";
    public static final String authorizationNumber = PREFIX + "authorizationNumber";
    public static final String birthDate = PREFIX + "birthDate";
    public static final String crime = PREFIX + "crime";
    public static final String criminalAgency = PREFIX + "criminalAgency";
    public static final String currency = PREFIX + "currency";
    public static final String date = PREFIX + "date";
    public static final String declaredMoney = PREFIX + "declaredMoney";
    public static final String email = PREFIX + "email";
    public static final String firstName = PREFIX + "firstName";
    public static final String jobTitle = PREFIX + "jobTitle";
    public static final String lastName = PREFIX + "lastName";
    public static final String origin = PREFIX + "origin";
    public static final String passPortNumber = PREFIX + "passPortNumber";
    public static final String registerNumber = PREFIX + "registerNumber";
    public static final String reportFrom = PREFIX + "reportFrom";
    public static final String taxID = PREFIX + "taxID";
    public static final String transactionFrom = PREFIX + "transactionFrom";
    public static final String transactionID = PREFIX + "transactionID";

    /* individuals */
    public static final String Asia = PREFIX + "Asia";
    public static final String Europe = PREFIX + "Europe";
    public static final String NorthAmerica = PREFIX + "NorthAmerica";
    public static final String SouthAmerica = PREFIX + "SouthAmerica";
    public static final String FBI = PREFIX + "FBI";
    public static final String Interpol = PREFIX + "Interpol";

    public static class R {
        public static final Resource CriminalRecord = ResourceFactory.createResource(Security.CriminalRecord);
        public static final Resource FinancialTransaction = ResourceFactory.createResource(Security.FinancialTransaction);
        public static final Resource ImmigrationRecord = ResourceFactory.createResource(Security.ImmigrationRecord);
        public static final Resource Person = ResourceFactory.createResource(Security.Person);
        public static final Resource ImmigrationRecordEU = ResourceFactory.createResource(Security.ImmigrationRecordEU);
        public static final Resource ImmigrationRecordNA = ResourceFactory.createResource(Security.ImmigrationRecordNA);
        public static final Resource ImmigrationRecordSA = ResourceFactory.createResource(Security.ImmigrationRecordSA);
        public static final Resource ImmigrationRecordAS = ResourceFactory.createResource(Security.ImmigrationRecordAS);
        public static final Resource FinancialTransactionEU = ResourceFactory.createResource(Security.FinancialTransactionEU);
        public static final Resource FinancialTransactionNA = ResourceFactory.createResource(Security.FinancialTransactionNA);
        public static final Resource FinancialTransactionSA = ResourceFactory.createResource(Security.FinancialTransactionSA);
        public static final Resource FinancialTransactionAS = ResourceFactory.createResource(Security.FinancialTransactionAS);
        public static final Resource CriminalRecordFBI = ResourceFactory.createResource(Security.CriminalRecordFBI);
        public static final Resource CriminalRecordInterpol = ResourceFactory.createResource(Security.CriminalRecordInterpol);
        public static final Resource PersonWithImmigrationRecordEU = ResourceFactory.createResource(Security.PersonWithImmigrationRecordEU);
        public static final Resource PersonWithImmigrationRecordNA = ResourceFactory.createResource(Security.PersonWithImmigrationRecordNA);
        public static final Resource PersonWithImmigrationRecordSA = ResourceFactory.createResource(Security.PersonWithImmigrationRecordSA);
        public static final Resource PersonWithImmigrationRecordAS = ResourceFactory.createResource(Security.PersonWithImmigrationRecordAS);
        public static final Resource PersonWithFinancialTransactionEU = ResourceFactory.createResource(Security.PersonWithFinancialTransactionEU);
        public static final Resource PersonWithFinancialTransactionNA = ResourceFactory.createResource(Security.PersonWithFinancialTransactionNA);
        public static final Resource PersonWithFinancialTransactionSA = ResourceFactory.createResource(Security.PersonWithFinancialTransactionSA);
        public static final Resource PersonWithFinancialTransactionAS = ResourceFactory.createResource(Security.PersonWithFinancialTransactionAS);

        public static final Property amount = ResourceFactory.createProperty(Security.amount);
        public static final Property arrivalDate = ResourceFactory.createProperty(Security.arrivalDate);
        public static final Property authorizationNumber = ResourceFactory.createProperty(Security.authorizationNumber);
        public static final Property birthDate = ResourceFactory.createProperty(Security.birthDate);
        public static final Property crime = ResourceFactory.createProperty(Security.crime);
        public static final Property criminalAgency = ResourceFactory.createProperty(Security.criminalAgency);
        public static final Property currency = ResourceFactory.createProperty(Security.currency);
        public static final Property date = ResourceFactory.createProperty(Security.date);
        public static final Property declaredMoney = ResourceFactory.createProperty(Security.declaredMoney);
        public static final Property email = ResourceFactory.createProperty(Security.email);
        public static final Property firstName = ResourceFactory.createProperty(Security.firstName);
        public static final Property jobTitle = ResourceFactory.createProperty(Security.jobTitle);
        public static final Property lastName = ResourceFactory.createProperty(Security.lastName);
        public static final Property origin = ResourceFactory.createProperty(Security.origin);
        public static final Property passPortNumber = ResourceFactory.createProperty(Security.passPortNumber);
        public static final Property registerNumber = ResourceFactory.createProperty(Security.registerNumber);
        public static final Property reportFrom = ResourceFactory.createProperty(Security.reportFrom);
        public static final Property taxID = ResourceFactory.createProperty(Security.taxID);
        public static final Property transactionFrom = ResourceFactory.createProperty(Security.transactionFrom);
        public static final Property transactionID = ResourceFactory.createProperty(Security.transactionID);

        public static final Resource Asia = ResourceFactory.createResource(Security.Asia);
        public static final Resource Europe = ResourceFactory.createResource(Security.Europe);
        public static final Resource NorthAmerica = ResourceFactory.createResource(Security.NorthAmerica);
        public static final Resource SouthAmerica = ResourceFactory.createResource(Security.SouthAmerica);
        public static final Resource FBI = ResourceFactory.createResource(Security.FBI);
        public static final Resource Interpol = ResourceFactory.createResource(Security.Interpol);
    }
}
