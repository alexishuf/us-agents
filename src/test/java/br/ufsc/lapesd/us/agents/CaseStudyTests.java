package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFDesire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import br.ufsc.lapesd.us.agents.exception.FulfillmentException;
import br.ufsc.lapesd.us.agents.test_helpers.*;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.List;

/**
 * Test cases described in the paper case study. These are not the experiments, these are
 * sanity handlebars
 */
public class CaseStudyTests extends SecurityScenarioWithIO {

    private Desire des(String desire, String informationElement) {
        return new RDFDesire(desire, new RDFInformationElement(informationElement));
    }

    @DataProvider(name = "desires")
    public Object[][] dataDesires() {
        String prefix = "@prefix o: <http://alexishuf.bitbucket.org/2016/07/us-agents/examples/security/ontology.ttl#> .\n" +
                "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
                "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n\n";

        return new Object[][] {
            {
                prefix + "[ o:taxID \"911-12004-737-98\"^^xsd:string ].",
                des(SecurityDesires.GetInformation, Security.CriminalRecord),
                new String[][] {
                    {SecurityScenario.policeFBI, SecurityDesires.GetInformation, Security.CriminalRecordFBI},
                    {SecurityScenario.policeInterpol, SecurityDesires.GetInformation, Security.CriminalRecordInterpol}
                }
            }, {
                prefix + "[\n" +
                         "  o:date \"6/11/2011\" ;\n" +
                         "  o:crime \"Murder: Second-degree\" ;\n" +
                         "  o:country \"Brazil\" ;\n" +
                         "  o:registerNumber \"823-91060-54668-68\" ;\n" +
                         "  o:author [ o:taxID \"911-12004-737-98\" ]\n" +
                         "].",
                des(SecurityDesires.NotifyCrime, Security.CriminalRecord),
                new String[][] {
                        {SecurityScenario.policeFBI, SecurityDesires.Register, Security.CriminalRecordFBI},
                        {SecurityScenario.policeInterpol, SecurityDesires.Register, Security.CriminalRecordInterpol},
                        {SecurityScenario.persons, SecurityDesires.GetInformation, Security.Person},
                        {SecurityScenario.immigrationAS, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordAS},
                        {SecurityScenario.immigrationSA, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordSA},
                        {SecurityScenario.immigrationNA, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordNA},
                        {SecurityScenario.immigrationEU, SecurityDesires.Blacklist, Security.PersonWithImmigrationRecordEU},
                        {SecurityScenario.financialAS, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionAS},
                        {SecurityScenario.financialSA, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionSA},
                        {SecurityScenario.financialNA, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionNA},
                        {SecurityScenario.financialEU, SecurityDesires.FreezeAssets, Security.PersonWithFinancialTransactionEU},
                }
            }
        };
    }

    @Test(dataProvider = "desires")
    public void testFulfill(String inputTTL, Desire desire, String[][] executors)
            throws FulfillmentException {
        DefaultProviderSelector selector = new DefaultProviderSelector();
        createServiceAgents(selector);
        createComposingAgents(selector);
        final boolean calls[] = new boolean[executors.length];
        for (int i = 0; i < calls.length; i++) calls[i] = false;

        for (int i = 0; i < executors.length; i++) {
            final int idx = i;
            getExecutor(executors[i][0], executors[i][1], executors[i][2])
                    .setInterceptor((in, out) -> {calls[idx] = true; return null;});
        }

        Model output = ModelFactory.createDefaultModel();
        List<DesireProvider> providers = selector.select(desire);
        Assert.assertFalse(providers.isEmpty());
        for (DesireProvider provider : providers) {
            RDFData providerOutput = provider.fulfill(desire, RDFData.fromTurtle(inputTTL));
            providerOutput.with(output::add);
        }

        for (int i = 0; i < calls.length; i++)
            Assert.assertTrue(calls[i], "i=" + i);
    }
}
