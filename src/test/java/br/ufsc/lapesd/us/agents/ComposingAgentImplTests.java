package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.test_helpers.*;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFDesire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import br.ufsc.lapesd.us.agents.exception.FulfillmentException;
import br.ufsc.lapesd.us.agents.exception.MissingInputsException;
import br.ufsc.lapesd.us.agents.test_helpers.PoliceScenarioWithIO;
import br.ufsc.lapesd.us.agents.test_helpers.PoliceScenario;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.Arrays;
import java.util.List;

@Test
public class ComposingAgentImplTests {
    public static Desire getInf(String element) {
        return new RDFDesire(PoliceDesires.GetInformation, new RDFInformationElement(element));
    }

    private void testExecutorsImpl(PoliceScenario s, String inputTTL) throws FulfillmentException {
        List<Desire> related = Arrays.asList(getInf(br.ufsc.lapesd.us.agents.test_helpers.Police.PoliceReport),
                getInf(br.ufsc.lapesd.us.agents.test_helpers.Police.InvolvedParty),
                getInf(br.ufsc.lapesd.us.agents.test_helpers.Police.Location));
        DefaultProviderSelector selector = new DefaultProviderSelector();
        s.createServiceAgents(selector);
        ComposingAgentImpl agent;
        agent = new ComposingAgentImpl(getInf(br.ufsc.lapesd.us.agents.test_helpers.Police.PoliceOccurrence),
                s.getOntologyUrl(), related, selector);

        boolean calls[] = {false, false, false, false, false, false};
        s.getExecutor(PoliceScenario.homicideService, PoliceDesires.GetInformation, br.ufsc.lapesd.us.agents.test_helpers.Police.MurderReport)
                .setInterceptor((i, o) -> {calls[0] = true; return null;});
        s.getExecutor(PoliceScenario.latrocinioService, PoliceDesires.GetInformation, br.ufsc.lapesd.us.agents.test_helpers.Police.RobberyReport)
                .setInterceptor((i, o) -> {calls[1] = true; return null;});
        s.getExecutor(PoliceScenario.suspiciousdeathService, PoliceDesires.GetInformation, br.ufsc.lapesd.us.agents.test_helpers.Police.SuicideReport)
                .setInterceptor((i, o) -> {calls[2] = true; return null;});
        s.getExecutor(PoliceScenario.victimService, PoliceDesires.GetInformation, br.ufsc.lapesd.us.agents.test_helpers.Police.Victim)
                .setInterceptor((i, o) -> {calls[3] = true; return null;});
        s.getExecutor(PoliceScenario.authorService, PoliceDesires.GetInformation, br.ufsc.lapesd.us.agents.test_helpers.Police.Author)
                .setInterceptor((i, o) -> {calls[4] = true; return null;});
        s.getExecutor(PoliceScenario.locationService, PoliceDesires.GetInformation, br.ufsc.lapesd.us.agents.test_helpers.Police.Location)
                .setInterceptor((i, o) -> {calls[5] = true; return null;});

        RDFData input = RDFData.fromTurtle(inputTTL == null ? "" : inputTTL);
        RDFData output = agent.fulfill(getInf(br.ufsc.lapesd.us.agents.test_helpers.Police.PoliceOccurrence), input);
        Assert.assertNotNull(output);

        for (int i = 0; i < calls.length; i++) Assert.assertTrue(calls[i], "i=" + i);
    }

    public class Police extends PoliceScenario {
        @Test
        public void testExecutors() throws FulfillmentException {
            testExecutorsImpl(this, null);
        }
    }

    public class PoliceWithIO extends PoliceScenarioWithIO {
        @Test
        public void testExecutorsNoData() throws FulfillmentException {
            boolean caught = false;
            try {
                testExecutorsImpl(this, null);
            } catch (MissingInputsException e) {
                caught = true;
            }
            Assert.assertTrue(caught);
        }

        @Test
        public void testExecutors() throws FulfillmentException {
            testExecutorsImpl(this, "@prefix o: <http://alexishuf.bitbucket.org/2016/07/us-agents/examples/police/ontology.ttl#> .\n" +
                    "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
                    "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n" +
                    "\n" +
                    "[ \n" +
                    "    o:year \"2015\"^^xsd:positiveInteger ;\n" +
                    "    o:policeReportNumber \"123.465-7\"^^xsd:string ;\n" +
                    "    o:policeStationNumber \"25\"^^xsd:string\n" +
                    "].\n");

        }
    }
}
