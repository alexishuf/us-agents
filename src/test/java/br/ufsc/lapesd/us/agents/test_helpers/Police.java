package br.ufsc.lapesd.us.agents.test_helpers;

public class Police {
    public static String PREFIX = "http://alexishuf.bitbucket.org/2016/07/us-agents/examples/police/ontology.ttl#";

    public static String Author = PREFIX + "Author";
    public static String MurderReport = PREFIX + "MurderReport";
    public static String RobberyReport = PREFIX + "RobberyReport";
    public static String DeathByBatteryReport = PREFIX + "DeathByBatteryReport";
    public static String DeathByPoliceConfrontationReport = PREFIX + "DeathByPoliceConfrontationReport";
    public static String SuicideReport = PREFIX + "SuicideReport";
    public static String PoliceReport = PREFIX + "PoliceReport";
    public static String PoliceStation = PREFIX + "PoliceStation";
    public static String Location = PREFIX + "Location";
    public static String PoliceOccurrence = PREFIX + "PoliceOccurrence";
    public static String InvolvedParty = PREFIX + "InvolvedParty";
    public static String Person = PREFIX + "Person";
    public static String CoronerRecord = PREFIX + "CoronerRecord";
    public static String Victim = PREFIX + "Victim";
}
