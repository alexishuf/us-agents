package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.ioconstraints.DesireExecutorIOConstraints;
import br.ufsc.lapesd.us.agents.ioconstraints.IOConstraints;
import br.ufsc.lapesd.us.agents.test_helpers.PoliceDesires;
import br.ufsc.lapesd.us.agents.test_helpers.Police;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.DesireExecutor;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFDesire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import org.apache.commons.io.IOUtils;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static br.ufsc.lapesd.us.agents.test_helpers.PoliceScenario.homicideService;


public class IOConstraintsTests {

    private class DummyExecutor implements DesireExecutor {
        private RDFDesire desire;
        private Model descriptionModel;
        private Resource description;

        public DummyExecutor(String descriptionTTL, String descriptionIri) {
            desire = new RDFDesire(PoliceDesires.GetInformation,
                    new RDFInformationElement(Police.MurderReport));
            descriptionModel = ModelFactory.createDefaultModel();
            descriptionModel.read(IOUtils.toInputStream(descriptionTTL), null, "TTL");
            description = descriptionModel.createResource(descriptionIri);
        }

        @Override
        public Desire getDesire() {
            return desire;
        }

        @Override
        public Resource getServiceDescription() {
            return description;
        }

        @Override
        public void execute(Model inputData, Model output) throws Exception {
            /* pass */
        }
    }

    @DataProvider(name = "satisfiesInputs")
    public Object[][] dataSatisfiesInputs() {
        String descriptionIRI = homicideService + "/description#service";
        String prologue = "@prefix o: <http://alexishuf.bitbucket.org/2016/07/us-agents/examples/police/ontology.ttl#> .\n" +
                "@prefix sd: <http://alexishuf.bitbucket.org/2016/07/us-agents/sd.ttl#> .\n" +
                "@prefix sp: <http://spinrdf.org/sp#> .\n" +
                "@prefix ex: <" + homicideService + "/description#" + ">. \n" +
                "@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .\n" +
                "@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .\n\n";

        return new Object[][] {
                {prologue + "" +
                        "ex:service sd:outputConstraints [\n" +
                        "    a sp:Ask ;\n" +
                        "    sp:text \"\"\"\n" +
                        "        ASK WHERE {\n" +
                        "            ?victim a o:Victim .\n" +
                        "            ?victim o:id ?rg .\n" +
                        "        }\n" +
                        "    \"\"\"^^xsd:string\n" +
                        "].\n", descriptionIRI,
                prologue + "" +
                        "ex:service sd:inputConstraints [\n" +
                        "    a sp:Ask ;\n" +
                        "    sp:text \"\"\"\n" +
                        "        ASK WHERE {\n" +
                        "            ?victim o:id ?rg .\n" +
                        "    }\n" +
                        "    \"\"\"^^xsd:string\n" +
                        "].\n", descriptionIRI,
                true}
        };
    }

    @Test(dataProvider = "satisfiesInputs")
    public void testSatisfiesInputs(String aTTL, String aIRI, String bTTL, String bIRI,
                                    boolean satisfies) {
        DummyExecutor aExecutor = new DummyExecutor(aTTL, aIRI);
        DummyExecutor bExecutor = new DummyExecutor(bTTL, bIRI);
        IOConstraints a = new DesireExecutorIOConstraints(aExecutor);
        IOConstraints b = new DesireExecutorIOConstraints(bExecutor);

        RDFData example = a.exampleOutput(aExecutor.getDesire());
        Assert.assertEquals(b.validateInput(bExecutor.getDesire(), example).isValid(), satisfies);
    }
}
