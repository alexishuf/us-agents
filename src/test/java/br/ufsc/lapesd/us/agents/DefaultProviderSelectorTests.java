package br.ufsc.lapesd.us.agents;

import br.ufsc.lapesd.us.agents.desires.rdf.RDFDesire;
import br.ufsc.lapesd.us.agents.test_helpers.Hierarchy;
import br.ufsc.lapesd.us.agents.test_helpers.HierarchyScenario;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import br.ufsc.lapesd.us.agents.test_helpers.CrudDesires;
import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import java.util.*;
import java.util.stream.Collectors;

@Test
public class DefaultProviderSelectorTests {

    public class HierarchyTests extends HierarchyScenario {
        public Desire retrieve(String element) {
            return new RDFDesire(CrudDesires.Retrieve, new RDFInformationElement(element));
        }

        private class LoadedServiceAgentImpl extends ServiceAgentImpl {
            public LoadedServiceAgentImpl(String ontologyUrl, String entryPoint, long load) {
                super(ontologyUrl, entryPoint);
                for (int i = 0; i < load; i++) incLoad();
            }
        }

        @DataProvider(name = "select")
        public Object[][] dataSelect() throws Exception {
            lockedInit();
            Object[][] objects = new Object[11][3];
            for (int i = 0; i < 10; i++) {
                objects[i] = new Object[] {null, null, null};
            }

            // A -> A
            ArrayList<DesireProvider> list = new ArrayList<>();
            list.add(new ServiceAgentImpl(getOntologyUrl(), aService));
            objects[0][0] = list;
            objects[0][1] = retrieve(Hierarchy.A);
            objects[0][2] = list.stream().collect(Collectors.toList());

            // A -> AA AB
            list = new ArrayList<>();
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), abService));
            objects[1][0] = list;
            objects[1][1] = retrieve(Hierarchy.A);
            objects[1][2] = list.stream().collect(Collectors.toList());

            // A -> A | B C
            list = new ArrayList<>();
            list.add(new ServiceAgentImpl(getOntologyUrl(), aService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), bService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), cService));
            objects[2][0] = list;
            objects[2][1] = retrieve(Hierarchy.A);
            objects[2][2] = Collections.singletonList(list.get(0));

            // A -> AA AB | B
            list = new ArrayList<>();
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), abService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), bService));
            objects[3][0] = list;
            objects[3][1] = retrieve(Hierarchy.A);
            objects[3][2] = Arrays.asList(list.get(0), list.get(1));

            // AA -> AA | AB
            list = new ArrayList<>();
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), abService));
            objects[4][0] = list;
            objects[4][1] = retrieve(Hierarchy.AA);
            objects[4][2] = Collections.singletonList(list.get(0));

            // A -> AA AB | AAA
            list = new ArrayList<>();
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), abService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaaaService));
            objects[5][0] = list;
            objects[5][1] = retrieve(Hierarchy.A);
            objects[5][2] = Arrays.asList(list.get(0), list.get(1));

            // A -> AA | AAAB
            list = new ArrayList<>();
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaabService));
            objects[6][0] = list;
            objects[6][1] = retrieve(Hierarchy.A);
            objects[6][2] = Collections.singletonList(list.get(0));

            // A -> AAA AAB AB | AAAA
            list = new ArrayList<>();
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaaService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), aabService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), abService));
            list.add(new ServiceAgentImpl(getOntologyUrl(), aaaaService));
            objects[7][0] = list;
            objects[7][1] = retrieve(Hierarchy.A);
            objects[7][2] = Arrays.asList(list.get(0), list.get(1), list.get(2));

            /* ----- eliminate competition tests ----- */

            // A -> A(0) | A(1)
            list = new ArrayList<>();
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), aService, 0));
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), aService, 1));
            objects[8][0] = list;
            objects[8][1] = retrieve(Hierarchy.A);
            objects[8][2] = Collections.singletonList(list.get(0));

            // A -> AA(0) AB(0) | AA(1) AB(1)
            list = new ArrayList<>();
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), aaService, 0));
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), aaService, 1));
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), abService, 0));
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), abService, 1));
            objects[9][0] = list;
            objects[9][1] = retrieve(Hierarchy.A);
            objects[9][2] = Arrays.asList(list.get(0), list.get(2));

            // A -> A(3) | AA(0) AB(0)
            list = new ArrayList<>();
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), aService, 3));
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), aaService, 0));
            list.add(new LoadedServiceAgentImpl(getOntologyUrl(), abService, 0));
            objects[10][0] = list;
            objects[10][1] = retrieve(Hierarchy.A);
            objects[10][2] = Collections.singletonList(list.get(0));

            return objects;
        }

        @Test(dataProvider = "select")
        public void testSelect(List<DesireProvider> providers, Desire desire,
                               List<DesireProvider> expectedList) {
            DefaultProviderSelector selector = new DefaultProviderSelector();
            providers.forEach(selector::addProvider);

            HashSet<DesireProvider> actual = new HashSet<>(), expected = new HashSet<>();
            actual.addAll(selector.select(desire));
            expected.addAll(expectedList);

            Assert.assertEquals(actual, expected);
        }
    }
}
