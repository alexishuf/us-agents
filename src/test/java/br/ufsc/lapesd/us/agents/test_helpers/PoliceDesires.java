package br.ufsc.lapesd.us.agents.test_helpers;

public class PoliceDesires {
    public static String PREFIX = "http://alexishuf.bitbucket.org/2016/07/us-agents/examples/police/desires.ttl#";

    public static String GetInformation = PREFIX + "GetInformation";
    public static String Register = PREFIX + "Register";
}
