package br.ufsc.lapesd.us.agents.test_helpers;

import br.ufsc.lapesd.us.agents.ComposingAgentImpl;
import br.ufsc.lapesd.us.agents.ProviderSelector;
import br.ufsc.lapesd.us.agents.desires.DesireExtractor;
import br.ufsc.lapesd.us.agents.desires.Desire;
import br.ufsc.lapesd.us.agents.desires.DesireExecutor;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFComposingAgentImplFactory;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFDesire;
import br.ufsc.lapesd.us.agents.desires.rdf.RDFInformationElement;
import br.ufsc.lapesd.us.agents.exception.NoProviderException;
import com.google.common.base.Preconditions;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.Lang;
import org.apache.jena.riot.RDFDataMgr;
import org.testng.annotations.*;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collectors;

public abstract class AbstractScenario {
    private File consolidatedOntology;
    private HashMap<String, HashMap<String, HashMap<String, MockExecutor>>> executors;
    private HashMap<String, Supplier<List<DesireExecutor>>> executorsSuppliers;
    private boolean initialized = false;

    public AbstractScenario() {
    }

    protected void init(String... ontologyResources) throws IOException {
        executors = new HashMap<>();
        executorsSuppliers = new HashMap<>();
        consolidatedOntology = Files.createTempFile(null, null).toFile();
        ClassLoader loader = PoliceScenario.class.getClassLoader();
        Model model = ModelFactory.createDefaultModel();
        for (String path : ontologyResources) {
            String[] parts = path.split("\\.");
            String ext = parts[parts.length - 1].toUpperCase();
            model.read(loader.getResourceAsStream(path), null, ext);
        }
        consolidatedOntology.deleteOnExit();
        try (FileOutputStream out = new FileOutputStream(consolidatedOntology)) {
            RDFDataMgr.write(out, model, Lang.TURTLE);
        }
    }

    @BeforeMethod
    public void lockedInit() throws Exception {
        if (initialized) return;
        init();
        initialized = true;
    }

    public abstract void createServiceAgents(ProviderSelector selector);

    public abstract void createComposingAgents(ProviderSelector selector) throws NoProviderException;

    protected abstract void init() throws Exception;

    public void createComposingAgents(ProviderSelector selector, String... resources) throws NoProviderException {
        ClassLoader loader = getClass().getClassLoader();
        final RDFComposingAgentImplFactory fac = RDFComposingAgentImplFactory.getInstance();

        Model model = ModelFactory.createDefaultModel();
        for (String resourcePath : resources) {
            String[] parts = resourcePath.split("\\.");
            model.read(loader.getResourceAsStream(resourcePath), null, parts[parts.length - 1]);
        }

        List<Resource> compositeDesires = fac.getCompositeDesires(model);
        for (Resource compositeDesire : compositeDesires) {
            ComposingAgentImpl agent;
            agent = fac.createAgentImpl(compositeDesire, getOntologyUrl(), selector);
            selector.addProvider(agent);
        }
    }

    @AfterMethod
    public void tearDown() {
        if (!initialized) return;
        DesireExtractor.getInstance().clearForced();
        consolidatedOntology.delete();
        executors.clear();
        initialized = false;
    }

    @AfterClass
    public void afterClass() {
        tearDown();
    }

    public String getOntologyUrl() {
        //File.toURI() does file:<path>
        return "file://" + consolidatedOntology.getAbsolutePath();
    }

    public MockExecutor getExecutor(String serviceEntryPoint, String desireIri,
                                    String elementIri) {
        HashMap<String, HashMap<String, MockExecutor>> map2 = executors.get(serviceEntryPoint);
        if (map2 == null) return null;
        HashMap<String, MockExecutor> map3 = map2.get(desireIri);
        if (map3 == null) return null;
        return map3.get(elementIri);
    }

    protected AbstractScenario addExecutorSupplier(String entryPoint,
                                                   Supplier<List<DesireExecutor>> supp) {
        final DesireExtractor ex = DesireExtractor.getInstance();
        ex.forceExtractor(entryPoint, supp);
        executorsSuppliers.put(entryPoint, supp);
        return this;
    }

    protected void clearExecutorSuppliers() {
        executorsSuppliers.clear();
    }

    protected void wrapExecutorsSupplier(String entryPoint,
                                        Function<Supplier<List<DesireExecutor>>,
                                                 Supplier<List<DesireExecutor>>> wrapperFactory) {
        Supplier<List<DesireExecutor>> old = executorsSuppliers.get(entryPoint);
        final DesireExtractor ex = DesireExtractor.getInstance();
        ex.forceExtractor(entryPoint, wrapperFactory.apply(old));
    }

    protected void wrapExecutors(String entryPoint,
                                 Function<DesireExecutor, DesireExecutor> mapper) {
        wrapExecutorsSupplier(entryPoint, (supp) -> () -> supp.get().stream().map(mapper)
                .collect(Collectors.toList()));
    }

    public class MockExecutor implements DesireExecutor {
        public final String serviceEntryPoint, desireIri, elementIri;
        private final RDFDesire desire;
        private Model descriptionModel = null;
        private Resource description = null;
        private BiFunction<Model, Model, Void> interceptor = null;

        public MockExecutor(String serviceEntryPoint, String desireIri, String elementIri) {
            this.serviceEntryPoint = serviceEntryPoint;
            this.desireIri = desireIri;
            this.elementIri = elementIri;
            this.desire = new RDFDesire(desireIri, new RDFInformationElement(elementIri));

            HashMap<String, HashMap<String, MockExecutor>> map2 = executors.get(serviceEntryPoint);
            if (map2 == null) {
                map2 = new HashMap<>();
                executors.put(serviceEntryPoint, map2);
            }
            HashMap<String, MockExecutor> map3 = map2.get(desireIri);
            if (map3 == null) {
                map3 = new HashMap<>();
                map2.put(desireIri, map3);
            }
            map3.put(elementIri, this);
        }

        public MockExecutor setInterceptor(BiFunction<Model, Model, Void> interceptor) {
            this.interceptor = interceptor;
            return this;
        }

        public MockExecutor setDescription(Model model, Resource description) {
            if (descriptionModel != null)
                descriptionModel.close();
            if (model != null && description != null) {
                Preconditions.checkArgument(description.getModel() == model);
                this.description = description;
                this.descriptionModel = model;
            } else {
                Preconditions.checkArgument(model == null && description == null);
            }
            return this;
        }

        @Override
        public Desire getDesire() {
            return desire;
        }

        @Override
        public Resource getServiceDescription() {
            return description;
        }

        @Override
        public void execute(Model inputData, Model output) throws Exception {
            if (interceptor != null)
                interceptor.apply(inputData, output);
        }
    }
}
